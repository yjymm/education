FROM centos

MAINTAINER yjymm <852093344@qq.com>

COPY app.jar /usr/local/app.jar

#把java与tomcat添加到容器中
ADD jdk-8u241-linux-x64.tar.gz /usr/local/

#设置工作访问时候的WORKDIR路径，登录落脚点
ENV MYPATH /usr/local

WORKDIR $MYPATH

#配置java与tomcat环境变量
ENV JAVA_HOME /usr/local/jdk1.8.0_241

ENV PATH $PATH:$JAVA_HOME/bin

#容器运行时监听的端口
EXPOSE 8080

CMD java -jar /usr/local/app.jar

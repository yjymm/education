package com.yjymm.edu;

import com.yjymm.edu.common.RedisUtils;
import com.yjymm.edu.mapper.OrderTableMapper;
import com.yjymm.edu.model.dto.OrderDTO;
import com.yjymm.edu.model.entity.OrderTable;
import com.yjymm.edu.service.OrderTableService;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.redis.core.RedisTemplate;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.sql.SQLException;
import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.Set;

/**
 * @author yjymm
 * @date 2021-04-07 16:55
 */
@SpringBootTest
public class OrderTest {

    @Resource
    OrderTableService orderTableService;

    @Resource
    RedisTemplate<String, Object> redisTemplate;

    @Resource
    RedisUtils redisUtils;

    @Test
    public void testAddOrder () throws SQLException {
        OrderDTO orderDTO = null;

        for (int i = 0; i < 20; i++) {
            orderDTO = new OrderDTO();
            orderDTO.setPid(11);

            orderDTO.setGids(Arrays.asList(i % 5 + 1, i % 4 + 2, i % 2 + 2));
            orderDTO.setSids(Arrays.asList(i % 6 + 1, i % 4 + 2,i % 3 + 2));
//            orderDTO.setMoney((double) (100 + i * 10));
            orderDTO.setMoney(BigDecimal.valueOf(100 + i*10));
            orderDTO.setFromTime(LocalDateTime.of(2021, 4, i+2, 17, 15, 0));
            orderDTO.setToTime(LocalDateTime.of(2021, 5, i+4, 17, 15, 0));

            orderDTO.setDescription("家教老师");
            orderDTO.setProvince("广东");
            orderDTO.setCity("深圳");
            orderDTO.setLocation("福田");
            orderDTO.setDetailLocation("豪华小区");
            orderTableService.dispatchOrder(orderDTO);
        }

    }

    @Test
    public void test (){
        Set<String> keys = redisTemplate.keys("*");
        for (String key : keys) {
            redisUtils.delete(key);
        }

    }
}

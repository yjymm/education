package com.yjymm.edu;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.yjymm.edu.common.RedisUtils;
import com.yjymm.edu.mapper.*;
import com.yjymm.edu.model.common.CommonResult;
import com.yjymm.edu.model.common.CommonResultEnum;
import com.yjymm.edu.model.dto.SearchOrderDTO;
import com.yjymm.edu.model.dto.UgtableDTO;
import com.yjymm.edu.model.dto.UserT;
import com.yjymm.edu.model.entity.Grade;
import com.yjymm.edu.model.entity.OrderTable;
import com.yjymm.edu.model.entity.User;
import com.yjymm.edu.model.entity.UserLog;
import com.yjymm.edu.model.vo.GradeVO;
import com.yjymm.edu.service.GradeService;
import com.yjymm.edu.service.OrderTableService;
import com.yjymm.edu.service.RoleService;
import com.yjymm.edu.service.UserService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.BeanUtils;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.web.method.annotation.ModelAttributeMethodProcessor;
import sun.rmi.runtime.Log;

import javax.annotation.Resource;
import javax.sql.DataSource;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@SpringBootTest
class EducationApplicationTests {

    @Resource
    UserLogMapper userLogMapper;

    @Resource
    private UgtableMapper ugtableMapper;
    @Resource
    UserService userService;

    @Resource
    UserMapper userMapper;

    @Resource
    RoleService roleService;

    @Resource
    RedisTemplate<String, Object> redisTemplate;

    @Resource
    private GradeService gradeService;

    @Resource
    OrderTableService orderService;

    @Resource
    DataSource dataSource;

    @Resource
    GradeMapper gradeMapper;

    @Resource
    RedisUtils redisUtils;

    @Test
    void contextLoads() {
        System.out.println(dataSource);
        System.out.println(gradeMapper);
    }

    @Test
    public void test() {
        GradeVO gradeVo = GradeVO.builder()
                .id(1)
                .gradeName("ssss")
                .build();

        System.out.println(gradeVo);
    }

    @Test
    public void testCopy() {
        User user = User.builder().build();
        UserT userT = new UserT();
        user.setId(1);
        user.setOpenId("111");
        user.setDeleted(1);
        user.setUsername("fsdfsdfdsf");
        // 多的属性塞到少的属性里面
        BeanUtils.copyProperties(user, userT);
        System.out.println(userT);

    }

    @Test
    public void testRedis() throws JsonProcessingException {
        User user = User.builder().build();
        user.setUsername("xxx");
        user.setPassword("aaa");
        ObjectMapper objectMapper = new ObjectMapper();
        String s = objectMapper.writeValueAsString(user);
        Map map = objectMapper.readValue(s, Map.class);
        redisTemplate.opsForHash().putAll("user", map);
//        User user1 = new User();
//        Object o = redisTemplate.opsForHash().get("user", "username");
//        System.out.println(o);
    }

    @Test
    public void testGradeList() {
        List<Grade> list = gradeService.list(null);
        System.out.println(list);
    }

    @Test
    public void testRoleService() {
        System.out.println(roleService);
    }

    @Test
    public void testUserPage() {
        // 从第0页开始
        Long page = Long.valueOf(1);
        long size = 2;
        IPage<User> page1 = new Page<User>(page, size);
        userService.page(page1, null);
        System.out.println(page1.getPages());   //
        System.out.println(page1.getRecords());
        System.out.println(page1.getCurrent());  // 当前页
        System.out.println(page1.getTotal());
        System.out.println(page1.getSize());
    }

    @Test
    public void testEnum() {
//        CommonResultEnum A = CommonResultEnum.LOGIN_ERROR;
//        System.out.println(A);
//        A.setMessage("sdsdfsdfds");
//        System.out.println(A);         // sdsdfsdfds
//        System.out.println(CommonResultEnum.LOGIN_ERROR); // sdsdfsdfds
    }

    @Test
    public void testUgDelete() {
        Integer uid = 1;
        List<Integer> gids = Arrays.asList(1, 2, 3);
        UgtableDTO ugtableDTO = new UgtableDTO();
        ugtableDTO.setUid(uid);
        ugtableDTO.setGids(gids);
        ugtableMapper.deleteUg(ugtableDTO);
    }


    @Test
    public void testUgInsert() {
        Integer uid = 1;
        List<Integer> gids = Arrays.asList(1, 2, 3);
        UgtableDTO ugtableDTO = new UgtableDTO();
        ugtableDTO.setUid(uid);
        ugtableDTO.setGids(gids);
        ugtableMapper.insertUg(ugtableDTO);
    }


    @Test
    public void testRedisMap() throws JsonProcessingException {
        User user = User.builder().build();
        user.setId(1);
        user.setPassword("password");
        user.setUsername("username");
        String key = "testusermap";
        redisUtils.insertObject(key, user);

        Object o = redisUtils.selectMap2Object(key, User.class);
        System.out.println(o);

    }

//    @Test
//    public void testOrderService (){
//        SearchOrderDTO dto = new SearchOrderDTO();
//        dto.setPid(1);
//        IPage orderList = orderService.getOrderList(dto);
//        System.out.println(orderList);
//        IPage<User> userIPage = userService.listPageNoWrapper(1L, 1L);
//        System.out.println(userIPage);
//    }


    @Test
    public void testList() {

    }

    public static void main(String[] args) {
        List<Integer> list = Arrays.asList(1, 2, 3, 4, 5);
        List<Integer> list1 = list.subList(5, 5);
        System.out.println(list1);
    }

    @Test
    public void testFetch (){
        Map<String, Object> map = new HashMap<>();
        map.put("uid", 11);
        List<UserLog> userLogs = userLogMapper.selectByMap(map);

    }
}

package com.yjymm.edu;

import com.yjymm.edu.common.JwtConfigProperties;
import com.yjymm.edu.common.JwtUtils;
import com.yjymm.edu.common.RsaUtils;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.util.ResourceUtils;

import javax.annotation.Resource;
import java.io.File;
import java.security.PublicKey;

/**
 * @author yjymm
 * @date 2021-01-14 23:52
 */
@SpringBootTest
public class JwtTest {

    @Resource
    JwtConfigProperties properties;

    @Test
    public void test (){
        System.out.println(properties.getPrivateKeyPath());
    }

    @Test
    public void testPublicKey () throws Exception {

        File file = ResourceUtils.getFile(properties.getPublicKeyPath());
        System.out.println(file.getAbsolutePath());
        PublicKey publicKeyByFile = RsaUtils.getPublicKeyByFile(file);
        System.out.println(publicKeyByFile);
    }

    /*public static void main(String[] args) throws Exception {
//        RsaUtils.generateKey("F:\\edit_workspace2\\idea2020.2.3\\education\\src\\main\\resources\\jwtkey\\id_rsa.pub",
//                "F:\\edit_workspace2\\idea2020.2.3\\education\\src\\main\\resources\\jwtkey\\id_rsa",
//                "yjymm",
//                64);
//        F:\edit_workspace2\idea2020.2.3\education\target\classes\jwtkey\id_rsa
        File file = new File("F:\\edit_workspace2\\idea2020.2.3\\education\\target\\classes\\jwtkey\\id_rsa.pub");
//        File file = new File("F:\\edit_workspace2\\idea2020.2.3\\education\\src\\main\\resources\\jwtkey\\id_rsa.pub");
        System.out.println(file.getAbsolutePath());
        PublicKey publicKeyByFile = RsaUtils.getPublicKeyByFile(file);
        System.out.println(publicKeyByFile);
    }*/

    public static void main(String[] args) {
//        File file = ResourceUtils.getFile(jwtConfigProperties.getPrivateKeyPath());
//        String token = "eyJhbGciOiJSUzI1NiJ9.eyJ1c2VyIjoie1widXNlcm5hbWVcIjpcImFkbWluXCIsXCJwYXNzd29yZFwiOlwiMTIzXCJ9IiwianRpIjoiTW1JeE1UUTBZbU10Wmprek5TMDBNbVl4TFRrM01EY3RNMll4WldJd1ltWm1NbVZqIiwiZXhwIjoxNjEwODk0NDk3fQ.QrqsBv9ttOt8TcZvq3eF-DnIhQ_DDhV0jvNEEsreX3ObdiLhYLmUba7zW49NPb2jlnPexeUYDoOyPLqly4AW2H_qq7SfEuUEh8in1qY0qx1hs_sNNDY4-dYuWwbPFH10yEBiE83b5CKX-Z2QTgQSyhNTzjL4laVhakPC20H9CVNwkB-3a66FW_G3A8eILsnwQegD9Efzp7AxcSG-FvPABD7-2fgMhibn31ysSaLbfyeVZECgFfjKOxjCD2qzL8ixVDmLV23K0OXI1S7JL3-iJY1gpf3TksA3YI6SkNaoozKcgzFdQjCIdTh30Z-qVI8JdV12rCgx0Y4oNJ1Lr2Ux7A";
//        JwtUtils.getInfoFromToken(token, )
    }
}

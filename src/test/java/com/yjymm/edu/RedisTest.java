package com.yjymm.edu;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.yjymm.edu.common.RedisUtils;
import com.yjymm.edu.constant.UstableConstant;
import com.yjymm.edu.model.entity.User;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.redis.core.RedisTemplate;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.util.List;
import java.util.Map;

/**
 * @author yjymm
 * @date 2021-04-07 0:03
 */

@SpringBootTest
public class RedisTest {
    @Resource
    RedisUtils redisUtils;

    @Resource
    RedisTemplate redisTemplate;



    @Test
    public void testList (){
        List range = redisTemplate.opsForList().range(UstableConstant.US_USER_INTERESTED_KEY + 11, 0, -1);
        System.out.println(range);

    }

    @Test
    public void testMap () throws JsonProcessingException {
        User user = User.builder()
                .id(1)
//                .balance(1.0)
                .balance(BigDecimal.valueOf(1.0))
                .build();
        ObjectMapper objectMapper = new ObjectMapper();
        objectMapper.setSerializationInclusion(JsonInclude.Include.NON_NULL);
        String s = objectMapper.writeValueAsString(user);
        Map map = objectMapper.readValue(s, Map.class);
        redisTemplate.opsForHash().putAll("xxxx",map);

        System.out.println("读取");
        Map xxxx = redisTemplate.opsForHash().entries("xxxx");
        System.out.println(xxxx);
    }

    @Test
    public void testgetUser (){


    }
}

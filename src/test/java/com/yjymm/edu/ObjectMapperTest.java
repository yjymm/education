package com.yjymm.edu;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.yjymm.edu.model.common.CommonResult;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.HashMap;
import java.util.Map;

/**
 * @author yjymm
 * @date 2021-01-13 23:19
 */

@SpringBootTest
public class ObjectMapperTest {

    @Test
    public void test (){

    }

    public static void main(String[] args) {
        CommonResult xxx = CommonResult.builder()
                .code(1)
                .message("xxx")
                .result(222)
                .build();
        ObjectMapper objectMapper = new ObjectMapper();
        Map map = objectMapper.convertValue(xxx, HashMap.class);
        System.out.println(map.getClass());
        System.out.println(map);

    }
}

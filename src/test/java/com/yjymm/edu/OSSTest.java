package com.yjymm.edu;

import com.aliyun.oss.OSS;
import com.aliyun.oss.model.PutObjectRequest;
import com.yjymm.edu.config.OSSConfig;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.util.ResourceUtils;

import javax.annotation.Resource;
import java.io.File;
import java.io.FileNotFoundException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

/**
 * @author yjymm
 * @date 2021-01-07 18:37
 */
@SpringBootTest
public class OSSTest {

    @Resource
    OSSConfig ossConfig;

    @Resource
    OSS ossClient;

    @Test
    public void test () throws FileNotFoundException {
        File file = new File("./");
        System.out.println(file.getAbsolutePath());
        String url = ResourceUtils.getURL("classpath:imgs/1.png").getPath();
        File file1 = new File(url);
//                Paths.get(url);
        PutObjectRequest putObjectRequest = new PutObjectRequest(ossConfig.getBucketName(), "111.png", file1);
        ossClient.putObject(putObjectRequest);
        System.out.println(url);

    }

    public static void main(String[] args) {
        File file = new File(System.getProperty("user.dir"));
        Path path = Paths.get("classpath:imgs/");
        System.out.println(path.getFileName());
        System.out.println(file.getAbsolutePath());
//        String url = ResourceUtils.getURL("classpath:").getPath();
    }
}

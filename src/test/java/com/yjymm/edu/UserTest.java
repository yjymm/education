package com.yjymm.edu;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.yjymm.edu.common.StringUtils;
import com.yjymm.edu.mapper.UgtableMapper;
import com.yjymm.edu.mapper.UserLogMapper;
import com.yjymm.edu.mapper.UserMapper;
import com.yjymm.edu.mapper.UstableMapper;
import com.yjymm.edu.model.common.RoleEnum;
import com.yjymm.edu.model.dto.OrderMapDTO;
import com.yjymm.edu.model.dto.SearchUserDTO;
import com.yjymm.edu.model.dto.UgtableDTO;
import com.yjymm.edu.model.dto.UstableDTO;
import com.yjymm.edu.model.entity.User;
import com.yjymm.edu.model.entity.UserLog;
import com.yjymm.edu.model.entity.Ustable;
import com.yjymm.edu.service.OrderMapService;
import com.yjymm.edu.service.SearchService;
import com.yjymm.edu.service.UgtableService;
import com.yjymm.edu.service.UstableService;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.sql.SQLException;
import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.List;
import java.util.Random;
import java.util.UUID;

/**
 * @author yjymm
 * @date 2021-01-07 14:50
 */

@SpringBootTest
public class UserTest {

    @Resource
    private SearchService searchService;
    @Resource
    private OrderMapService orderMapService;
    @Resource
    private UserLogMapper userLogMapper;
    @Resource
    private UgtableMapper ugtableMapper;

    @Resource
    private UstableMapper ustableMapper;

    @Resource
    private UgtableService ugtableService;
    @Resource
    private UstableService ustableService;

    @Resource
    UserMapper userMapper;

    @Test
    public void checkBalance (){
        Boolean aBoolean = userMapper.checkBalance(2, 10.0);
        System.out.println(aBoolean);
    }


    @Test
    public void freezeBalance (){
        Boolean aBoolean = userMapper.freezeBalance(1, 5.0);
        System.out.println(aBoolean);
    }

    @Test
    public void test (){
        String teacherName = "xxx";
        QueryWrapper<User> queryWrapper = new QueryWrapper<>();
        queryWrapper.like("username", teacherName).or()
                .like("real_name", teacherName);
        IPage<User> page = new Page<>(1, 10);
        page = userMapper.selectPage(page, queryWrapper);
        System.out.println(page);
    }

    /**
     * 批量添加教师
     * @throws SQLException
     */
    @Test
    public void testInsertteacher () throws SQLException {
        List<String> list = Arrays.asList("陈功", "张三", "陈凡", "谢逊", "张无忌", "潘璐", "徐功", "罗大佑");
//         List<String> list = Arrays.asList("陈昭", "王五", "李六", "刘大", "梁河", "李东", "林林", "张兴");

        for (int i = 0; i < 8; i++) {
            User user = User.builder()
                    .openId(UUID.randomUUID().toString())
                    .phone("13794943414")
                    .gender(1)
                    .username(list.get(i))
                    .email(UUID.randomUUID().toString().substring(0,5) + "@qq.com")
                    .roleId(2)
                    .balance(BigDecimal.valueOf(100.00))
                    .freezeBalance(BigDecimal.valueOf(100.00))
                    .avatar("https://thirdwx.qlogo.cn/mmopen/vi_32/Q0j4TwGTfTI21r1FgIAN8dicl7qT0JMHmTKia1QPdiasX2WpNKicKr1vUUcDIlZiam15mdtjS8qPfreylHTBzXUEqkg/132")
                    .country("中国")
                    .province("广东")
                    .city("广州")
                    .description("家教经验丰富，曾帮助过多名学生提高成绩，对于如何教导学生学习有着一套自己的方法论，" +
                            "欢饮随机咨询")
                    .build();
            userMapper.insert(user);
            List<Integer> gids = Arrays.asList(1,2,5);
//            List<Integer> gids = Arrays.asList(3,4);
            ugtableService.update(user.getId(), gids);
            List<Integer> sids = Arrays.asList(1,4,6);
//            List<Integer> sids = Arrays.asList(2,3,5);
            ustableService.update(user.getId(), sids);
        }
    }


    @Test
    public void testdeleteBatch (){
        List<Integer> uids = Arrays.asList(100,101,102);
        userMapper.deleteBatchById(uids);
    }

    @Test
    public void testUgtable (){
        String openid = "oiMDm5eaw6c7IGVcOk6-PLYMHxk0";
        Integer uid = 11;
        UgtableDTO ugtableDTO = new UgtableDTO();
        ugtableDTO.setUid(uid);
        ugtableDTO.setGids(Arrays.asList(1,3,5));
        ugtableMapper.insertUg(ugtableDTO);
    }

    @Test
    public void testUstable (){
        String openid = "oiMDm5eaw6c7IGVcOk6-PLYMHxk0";
        Integer uid = 11;
        UstableDTO ugtableDTO = new UstableDTO();
        ugtableDTO.setUid(uid);
        ugtableDTO.setSids(Arrays.asList(1,3,5,6));
        ustableMapper.insertUs(ugtableDTO);
    }

    @Test
    public void testSelect (){
        // 查询所有角色为教师的uid
        User user = new User();
        user.setRoleId(RoleEnum.TEACHER.getId());
        user.setUsername("王");
        user.setDeleted(0);
        List<Integer> uidList = userMapper.selectWithProp(user);
        System.out.println(uidList);
    }

    @Test
    public void testSe (){
        String teacher = "陈";
        List<Integer> integers = Arrays.asList(25, 27);
        List<Integer> ids = userMapper.findUserIdsWithTeacherName(teacher, integers);
        System.out.println(ids);
    }

    @Test
    public void testmsg (){
        for (int i = 0; i < 10; i++) {
            UserLog log = UserLog.builder()
                    .uid(17)
                    .state(0)
                    .description("测试消息")
                    .createTime(LocalDateTime.now())
                    .build();
            userLogMapper.insert(log);
        }
    }

    @Test
    public void testjiedan () throws Exception {
        for (int i = 0; i < 5; i++) {
            OrderMapDTO orderMapDTO = new OrderMapDTO();
            orderMapDTO.setOid(7);
            orderMapDTO.setTid(20+i);
            orderMapDTO.setState(0);
            orderMapService.insert(orderMapDTO);
        }

    }

    public static void main(String[] args) {
        BigDecimal bigDecimal = BigDecimal.valueOf(400);
        BigDecimal bigDecimal1 = BigDecimal.valueOf(400.00);
        System.out.println(bigDecimal.subtract(bigDecimal1).equals(BigDecimal.valueOf(0.0)));

    }

    @Test
    public void testsearchUser (){
        SearchUserDTO dto = new SearchUserDTO();
        dto.setPage(1L);
        dto.setRoleId(2);
        dto.setUsername("陈");
        dto.setSize(10L);
        IPage iPage = searchService.searchUser(dto);
        System.out.println(iPage);

    }
}

DROP DATABASE IF EXISTS education;
CREATE DATABASE education;
USE education;
SET NAMES utf8;

DROP TABLE IF EXISTS role;
CREATE TABLE role
(
    id          INT PRIMARY KEY AUTO_INCREMENT,
    role_name   VARCHAR(50) NOT NULL,
    description VARCHAR(255),
    deleted     TINYINT DEFAULT 0
) ENGINE = INNODB
  DEFAULT CHARSET = utf8;

INSERT INTO role (id, role_name, description)
VALUES (1, 'ROLE_ADMIN', '后台管理员'),
       (2, 'ROLE_TEACHER', '家庭教师'),
       (3, 'ROLE_USER', '普通用户'),
       (4, 'ROLE_PARENT', '家长');

#后续parent角色可以关联student角色，达到监督学习的效果
#可以理解为：
#	家长想找这些标签的东西
#	教师具有这些东西的技能
DROP TABLE IF EXISTS `user`;
CREATE TABLE `user`
(
    id             INT PRIMARY KEY AUTO_INCREMENT,
    deleted        TINYINT DEFAULT 0,
    open_id        VARCHAR(255) NOT NULL COMMENT '微信小程序openId，唯一标识',
    gender         TINYINT DEFAULT 1 COMMENT '1:男，2：女',
    phone          VARCHAR(11),
    username       VARCHAR(255) COMMENT '用户账号的用户名',
    `password`     VARCHAR(255) COMMENT '密码',
    real_name      VARCHAR(255) COMMENT '该用户的真实姓名',
    email          VARCHAR(255),
    role_id        INT COMMENT '用户的角色',
    balance        decimal  DEFAULT 0 COMMENT '用户账户余额，可以提现',
    freeze_balance decimal  DEFAULT 0 COMMENT '用户冻结余额，不能体现',
    avatar         varchar(255) comment '用户头像',
    country        varchar(255) comment '国家',
    province       varchar(255) comment '省份',
    city           varchar(255) comment '城市',
    description       text comment '个人描述'
) ENGINE = INNODB
  DEFAULT CHARSET = utf8;

#年级表
DROP TABLE IF EXISTS grade;
CREATE TABLE grade
(
    id         INT PRIMARY KEY AUTO_INCREMENT,
    deleted    TINYINT DEFAULT 0,
    grade_name VARCHAR(50) COMMENT '年级名称：小学，初中，高中，大学'
) ENGINE = INNODB
  DEFAULT CHARSET = utf8;

INSERT INTO grade
VALUES (1, 0, '小学'),
       (2, 0, '初中'),
       (3, 0, '高中'),
       (4, 0, '大学'),
       (5, 0, '研究生');


#用户年级关联表，
#家长关联表示想寻找相关年级的教师
#教师关联表示教师有相关年级教授经验
DROP TABLE IF EXISTS ugtable;
CREATE TABLE ugtable
(
    id      INT PRIMARY KEY AUTO_INCREMENT,
    deleted TINYINT DEFAULT 0,
    uid     INT NOT NULL,
    gid     INT NOT NULL
) ENGINE = INNODB
  DEFAULT CHARSET = utf8;


#科目表
DROP TABLE IF EXISTS `subject`;
CREATE TABLE `subject`
(
    id           INT PRIMARY KEY AUTO_INCREMENT,
    deleted      TINYINT DEFAULT 0,
    subject_name VARCHAR(50) NOT NULL UNIQUE COMMENT '语文，数学，英语，物理，生物，吉他等'
) ENGINE = INNODB
  DEFAULT CHARSET = utf8;

INSERT INTO `subject`
VALUES (1, 0, '语文'),
       (2, 0, '数学'),
       (3, 0, '英语'),
       (4, 0, '物理'),
       (5, 0, '生物'),
       (6, 0, '吉他');


#用户科目关联表，
#家长关联表示想寻找相关科目的教师
#教师关联表示教师有相关科目教授经验
DROP TABLE IF EXISTS ustable;
CREATE TABLE ustable
(
    id      INT PRIMARY KEY AUTO_INCREMENT,
    deleted TINYINT DEFAULT 0,
    uid     INT NOT NULL,
    sid     INT NOT NULL
) ENGINE = INNODB
  DEFAULT CHARSET = utf8;
ALTER TABLE ugtable
    add unique index uq_u_g (uid, gid);

#订单--------------

#用户充值记录表
DROP TABLE IF EXISTS recharge;
CREATE TABLE recharge
(
    id            INT PRIMARY KEY AUTO_INCREMENT,
    deleted       TINYINT DEFAULT 0,
    recharge_num  varchar(255) comment '充值单号',
    create_time   DATETIME COMMENT '创建充值订单时间',
    recharge_time DATETIME COMMENT '充值时间',
    finished      TINYINT DEFAULT 0 COMMENT '充值是否完成 0：未完成，1：已完成，2：已取消，3：已经超时',
    finish_time   DATETIME COMMENT '完成时间',
    uid           INT,
    money         decimal
) ENGINE = INNODB
  DEFAULT CHARSET = utf8;



#用户订单表,只有家长才能发起订单，教师抢购订单
#家长的订单，教师的订单
DROP TABLE IF EXISTS `order_table`;
CREATE TABLE `order_table`
(
    `id`          int(11)     NOT NULL AUTO_INCREMENT,
    `order_num`   varchar(255) NOT NULL DEFAULT '' COMMENT '订单号',
    `deleted`     tinyint(4)           DEFAULT 0,
    `pid`         int(11)              DEFAULT NULL COMMENT '对应user表的家长角色的id，哪个家长发起的订单',
    `create_time` datetime             DEFAULT NULL COMMENT '订单创建时间',
    `from_time`   datetime             DEFAULT NULL COMMENT '订单业务开始时间',
    `to_time`     datetime             DEFAULT NULL COMMENT '订单业务结束时间',
    `money`       decimal               DEFAULT 0 COMMENT '订单总金额，家长会冻结相关金额，教师会收到相关金额',
    `finished`    tinyint(4)           DEFAULT 0 COMMENT '0:未完成(默认)，1：已取消，2：进行中，3：已完成',
    `description` text COMMENT '家长对于此订单的描述,要求',
    `gids`        varchar(50) comment '此订单关联的年级，以 ;分割',
    `sids`        varchar(50) comment '此订单关联的科目，以 ;分割',
    `username`       VARCHAR(255) default '' COMMENT '用户账号的用户名, 冗余字段',
    real_name      VARCHAR(255) default '' COMMENT '该用户的真实姓名,冗余字段',
    country        varchar(255) default '中国' comment '国家',
    province       varchar(255) comment '省份',
    city           varchar(255) comment '城市',
    location       varchar(255) comment '区域',
    detail_location       text comment '详细地址',
    PRIMARY KEY (`id`)
) ENGINE = InnoDB
  AUTO_INCREMENT = 2
  DEFAULT CHARSET = utf8;

#订单评价
DROP TABLE IF EXISTS `order_evaluate`;
CREATE TABLE `order_evaluate`
(
    id          INT PRIMARY KEY AUTO_INCREMENT,
    deleted     TINYINT DEFAULT 0,
    oid         int COMMENT '订单号',
    star        int     default 5 comment '订单评价星星',
    description text comment '评价描述',
    imgs        text comment '订单评价图片，以;号分割'
) ENGINE = INNODB
  DEFAULT CHARSET = utf8;

#订单评价图片
DROP TABLE IF EXISTS `order_evaluate_img`;
CREATE TABLE `order_evaluate_img`
(
    id      INT PRIMARY KEY AUTO_INCREMENT,
    deleted TINYINT DEFAULT 0,
    eid     int comment '评价id',
    img     varchar(255) comment '评价图片'
) ENGINE = INNODB
  DEFAULT CHARSET = utf8;


DROP TABLE IF EXISTS order_map;
CREATE TABLE order_map
(
    id            INT PRIMARY KEY AUTO_INCREMENT,
    deleted       TINYINT DEFAULT 0,
    oid           INT COMMENT '订单id',
    tid           INT COMMENT '教师id，表示这个教师抢购这个订单，可以多个教师抢购同一个订单',
    state         TINYINT DEFAULT 0 COMMENT '0：抢订单，1：被家长选中，2：被家长拒绝',
    reject_reason TEXT COMMENT '被家长拒绝的原因',
    create_time   datetime not null COMMENT '订单抢占时间'
) ENGINE = INNODB
  DEFAULT CHARSET = utf8;
ALTER TABLE order_map
    add UNIQUE INDEX om_oid_tid (oid, tid);


#用户提现记录表：
DROP TABLE IF EXISTS fetch_log;
create table fetch_log(
                          id            INT PRIMARY KEY AUTO_INCREMENT,
                          deleted       TINYINT DEFAULT 0,
                          uid           int,
                          fetch_money   decimal comment '提现金额，不能小于等于0',
                          fetch_num   varchar(255) comment '提现订单号',
                          fetch_time    datetime comment '提现时间',
                          finish_time    datetime comment '订单完成时间',
                          state         int comment '提现状态（0：发起提现，提现审核中，1：提现成功，2：提现失败，3：用户取消提现）',
                          result        text comment '提现结果'
)ENGINE = INNODB
 DEFAULT CHARSET = utf8;
ALTER TABLE fetch_log
    add INDEX fetch_log_fetch_time_index (fetch_time);



#管理员消息表
DROP TABLE IF EXISTS admin_log;
create table admin_log(
                          id            INT PRIMARY KEY AUTO_INCREMENT,
                          deleted       TINYINT DEFAULT 0,
                          create_time   datetime comment '消息创建时间',
                          log_time      datetime comment '提现时间',
                          state         int default 0 comment '0：未读，1：已读',
                          description   text comment '消息描述',
                          type          TINYINT DEFAULT 0 comment '消息类型 0：普通消息， 1：审核消息， 2：警告消息'
)ENGINE = INNODB
 DEFAULT CHARSET = utf8;
ALTER TABLE admin_log
    add INDEX admin_log_time_index (create_time);


#用户消息表
DROP TABLE IF EXISTS user_log;
create table user_log(
                          id            INT PRIMARY KEY AUTO_INCREMENT,
                          uid           INT comment '用户id，消息属于哪个用户',
                          deleted       TINYINT DEFAULT 0,
                          create_time   datetime comment '消息创建时间',
                          state         int default 0 comment '0：未读，1：已读',
                          description   text comment '消息描述'
)ENGINE = INNODB
 DEFAULT CHARSET = utf8;
ALTER TABLE user_log
    add INDEX user_log_time_index (create_time);


#用户余额变更表，用作展示使用
DROP TABLE IF EXISTS money_log;
create table money_log(
                         id            INT PRIMARY KEY AUTO_INCREMENT,
                         uid           INT comment '用户id，消息属于哪个用户',
                         deleted       TINYINT DEFAULT 0,
                         balance        decimal COMMENT '用户账户当前余额：变更之后的余额',
                         freeze_balance  decimal COMMENT '用户账户当前冻结余额：变更之后的冻结余额',
                         money          decimal COMMENT '用户变更的金额',
                         create_time   datetime comment '消息创建时间',
                         state         int default 0 comment '0：未读，1：已读',
                         description   text comment '消息描述'
)ENGINE = INNODB
 DEFAULT CHARSET = utf8;
ALTER TABLE money_log
    add INDEX money_log_time_index (create_time);


#教师审核记录表
DROP TABLE IF EXISTS teacher_check;
create table teacher_check(
                         id            INT PRIMARY KEY AUTO_INCREMENT,
                         check_num     varchar(255) comment '申请订单号',
                         uid           INT comment '用户id，消息属于哪个用户',
                         deleted       TINYINT DEFAULT 0,
                         create_time   datetime comment '消息创建时间',
                         state         int default 0 comment '0：申请，1：审核通过，2：拒绝通过',
                         description   text comment '审核结果说明',
                         ext           text comment '额外属性，用作扩展',
                         id_card_img          text comment '上传身份证照片信息'
)ENGINE = INNODB
 DEFAULT CHARSET = utf8;
ALTER TABLE teacher_check
    add INDEX teacher_check_time_index (create_time);
package com.yjymm.edu.config;

import org.springframework.context.annotation.Configuration;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

/**
 * @author yjymm
 * @date 2021-01-07 11:11
 */
@Configuration
@EnableSwagger2
public class SwaggerConfig {
}

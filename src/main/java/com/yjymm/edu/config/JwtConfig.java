package com.yjymm.edu.config;

import com.yjymm.edu.common.JwtConfigProperties;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Configuration;

/**
 * @author yjymm
 * @date 2021-01-14 23:51
 */
@EnableConfigurationProperties(JwtConfigProperties.class)
@Configuration
public class JwtConfig {
}

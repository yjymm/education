package com.yjymm.edu.config;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;

/**
 * @author yjymm
 * @date 2020-12-26 22:48
 */

@ConfigurationProperties(prefix = "wx")
@Data
public class WxConfigProperty {
    
    private String appid;

    private String appserect;

    private String grant_type;
    
    private String loginUrl;
    

}

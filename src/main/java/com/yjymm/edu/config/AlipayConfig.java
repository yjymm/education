package com.yjymm.edu.config;

public class AlipayConfig {
    /**
     * 商户appId
     */
    public static final String APP_ID = "2021000117604350";
    /**
     * 支付宝公钥
     */
    public static final String ALIPAY_PUBLIC_KEY = "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAh5p1skSzd4Qowv7P9lCV+p1HYoPDfwN3uoOvj4HrxKJ+itxxb6KRVn+fi4V5SlhE1I41tKDaDwJvj37Qunp7I7kDc3B1Z2EoWRCjbpQ9KYEo4pL5XpfNZ9XnWWggjKwg6Zfy7G4ye1YppM2B8mi+hiZzY7VhLMUCgX62F6bqO6ZgSksAlbIMnzlVSb8ZKx8f8nDWmvDTrUzDuCjrGkwYxKZnZaOesUrm3eTfbeY++YGh74XW/am+x4o5mxQxcFEiHiZY5AFOuOuRAy2tHzcOCt2H54m0N7decdCLEt8V5Dwhh0/SPmQMD4iu0cpGAL76rKKwIJu93i3Uu8xBQ3vJWwIDAQAB/gHMV97ns59BNSdqtDewAc0RCejrRwaW49mA7+M3Cm9S8aed2YG/3KXaklr0FUmQmbDm8RuXoMle0QMsrwmef2wpnSKPfijwjZAJwG/IsF5v2GCpYErlvOj1pGc+ZkxbWoGVmngPIXGkpUTBHFA2orkSH0bcWfkkwe+6aZ0KX70dTN9DhqQPLbCCM9/yLPD9rw/feXERO86StrzHlnv0KLLKe0uhTCM73TSt3UJ0o3tlAyVVBkz+8VwjcOpic8wye84S82203o41kxXyQYNzQfRBZI48Bmx6v4AAVy6xrznOObn+fEig/y7Ny+vgkhvK/GWtm6EwIDAQAB";
    /**
     * 私钥 pkcs8格式的
     */
    public static final String APP_PRIVATE_KEY = "MIIEvwIBADANBgkqhkiG9w0BAQEFAASCBKkwggSlAgEAAoIBAQCbkOgaRpYxZuz+FGoO3rxdQOg6ulksKgftDl2R8yeC1S9DJsqQvpkvb8flf+14P1epLqcLrm3ERc/wIEOZZ6DLWtI7bPAGjJ5koJ7QizlxStTjv+KecOTiWc5PDCoqxiNO+b17N1ZnwfUOOz6wU4HjQFje6qTFWKtLFGVYp9/A+33jjP56WhfYsiTZu2ermeyOPzm2ZCC+rtEJFVJuCGnK1W7S9pvhpk7U/6wfws2Y2uUvUbHrHKW//pqKKWeId6x+PJb7+1pMsWXXmW0wJMnMZfWsHHYRuRl3UIm78kEEoEcfJMmRwbEE5KfVzvOb0Q6fPXnZJrk6mzAVutwgBaLVAgMBAAECggEBAJpbmjIYQAcc3aLulp3SnIzbTpKXPMv5mIu7aVTcNCQ5a//xiaU9ubm77FjoKrTaBR010UJh/UB3FPBq0yhwDrg4kcmtOi0FclTemzbKLmEfo0WorDASTMnb2j3NEDoJCBfg7gZIzk99wK4yUu2arpLS0lu972hxpuWEg1sIl5hfY54lRJutrSLZOCjJj4EqD9R0xgZICylU+tkRfJsE+JmytgtKO15u3omr+R4bOMUV/S4NyP4Od5qbYy9t0KQO9pkMrQ8oDxtsWmWl4ACyXTWZqqm6mNxQniSh7WmCFWEuX5CmBoJfc94/zqbe+tcuNiy8DK1V96XH7GUqKDd8TbECgYEA4NDBYDk/YChooktx+OwJuB0cqGsnc+L4JFwVJcvKI0cb+4vKwXmo4BTRgkMSsEJpKO2Aj+wJxt9SnHvd89RaPfcK3QSmbZ41OXvXcLU1UKrkHoeYYDWPZDQdDcj0/LRTEjxq8/+lllIeXAiQP2QMk+aTI0cMrzlVEnwIr2CShu8CgYEAsSUXCShLMxSeP7OdydmeV4Av1SLOYIrmKr0bzcmGpoONzOsMxhL6f/XKCGAoqayd+cfcKNTLg56BtFk3H0q8t/TkIeG05Nt5/jYvJ3WmnqgB3OyX2n90Qr60fedu1su2WMbtFBnLZEsc6soOv1euwkOv/F23cV6j0V1T1CX+EnsCgYEAjlNauSJE/wlWBLkitGYqWQcFJ5J+7ZzZX5t0P/NeSysvifacLBRrV0GWyZLQ+AlzLdf+Nh9GqUKkuDCq+TyMzQHjPL0+59Lq+zNt1wrBcApgVPue+fmuh89+PUXl4z3+AWOCimpkWGAwnC9HsvoSBGhK6mCGpT/0nbksfmrzGL0CgYBBbD1RuHvtZZP8Ws3PmnuMPV9tC+nEZggTiLwYCHGeVb+tCrIxBRlW4OO4rCrYX1QIx/NbcRuohIXKoZhmCiggmGUYzk8MprIrXL+VhzH+LnZhrU939tJtRBlM1ohs89doei9c2Rnw9Hy9idpgeHWmcXExdzskt5sw25L2KwHmuQKBgQCzvxRmzJLu8Ven4UgvtBsdkKJzZfUIzfWnZMU69vXLFDdzytNyxTYYSF/ahpZfGeIFRWytVI/6JO7dWAXqfiVTgPjw1R/z7kbG2kwNOTGW5G7rrz142ZxrJJ3hQcfGffvWVogu8ek35w5EzzenqzfQdxEeB9LQpxxPC9IAD5qTiQ==/qWz7RJu2jXV01nljGFs4ECqF8itxU4L+OlGsKjYnrUDthKKFM9AWTaVIT861Ph9T5Jo83RZC8wBbtU5dYz1yfBD/U2BlFf9wqqjyJ5iifWUmRfs0jxym2RUJI/ApOVLVjF//02zHXnOL9xYoBj0HgoAN4w0rkxdSV1q+qdC4IxC1liTZ1amhRd2Galqcesa0GSSNBMOiUV+dU1/euUdxQ/e/DYhZYTu96q1xE2rQx0PWZqMoUlJrRZsniMTBodHYFzcatPqzsDxJqD9BlVFUWbCR+wLwatFQc/eiB4NGLA/ZZEx4AkHhB7vGTOjAgMBAAECggEAc+69ddPxC2EXXqe3BRryep6iWjqtsE3QbzUUnzQcakZH3RNYvvhVMnk/mON2G+kY36aXqb3tvOns+7B1VRVAi7jfsYgs0Yz2kqNNu3VMbWkuVPTMmxFWTA8KBkSgJl0xPhWi2Kqgidl3+w02eTGsGtVtu+tgWQYlL08zjW21fiTTmWHDieOAOUTpG8rHvHqQ/oiGoSCVj05/rrux7gzAWR1Cexdk6UtlreU/xBq7Gp1+kGw0cyHByirqTOLMIBmdX1MIospgo2LaXySbhfdmKILNjyYl7VaKGRkNgYbs6aebX6TLThJJyqGgTrLP3tlMiq60ODTifrzNMpjwzmZI0QKBgQDnNxwXe81XLYte+ypWqtv347GuK/LQEPEeaPvuDb7AhH2yXpjI7J2Fax96i8R49EnEj81kvYYp+aS/jlWGB6rc+AEuyn1unBmHSaYHE1CbzX8iJWG9TCZx3I1u6P7S0F6JhIqvsznncCzBnbRba2fgn8aWi7WZarb4FslQ+kWovQKBgQCwVixTHFkRH7Uh/WBxYYBaTQbD2b1i99+8QMfCorFbbKiSMtqXGnOadOTfr71F+l2bTS67VFejPFg+4UxakAoQQ7lncFIicVJlyY4tZYApyDkR0UGesm0E2MI5XZ2EGKXAUPnUExeijTQRt4FxOJiK78FxuWQeRDJZnNgxasUD3wKBgEJ6cuZBn0GQg8D0YDD5ATPd0KluU2sFY/5FWyIP66d4sJdHYZBrj8LbkPpwEzZfrpIIcpqjb17EO10kNGzL6ZGM8PIeIEBcO27NeyIPqJ6TUBlmPBrsOSWFr1iTPykn5faOGD36PwzPEN8+U6vxTrWj2rpQQw9wQllEW++fdcXVAoGBAJwPV+EiOI1YI4V2ajFOGjtQ6kjQ3rTFLSS9rE1MkWyZSMNC8IREa8Vp6Nky/m79OnTkpRQze/BCytKzsFCCI/XrCh5KoNZMtqDQ9dyldP0Fb8SeDtY1kRh2AcalXG95JNCEmeYgE43QLCH150Bd11s2/1v3uGy07F602f06Qiy/AoGBAJ0FCedSusHUi38gTbgbi35cflxzUhYSTzWlKCfMrOrqZEtWRyMwK1KnoDcNy5LvWMXGasS/qbouFwfKonSFl4CUggMBM0TV5P7qDIYxOZlJab0UvjWZSbiuC3RhJl9NyfF+kSPx9CgK5Zi0bp9FC04fAHw7Erbd0w4Leel94s4j";

    /**
     * 服务器异步通知页面路径 需http://或者https://格式的完整路径，不能加?id=123这类自定义参数，必须外网可以正常访问
     */
    public static final String NOTIFY_URL = "http://book.yjymm.top/alipay/notify";
    /**
     * 页面跳转同步通知页面路径 需http://或者https://格式的完整路径，不能加?id=123这类自定义参数，必须外网可以正常访问 商户可以自定义同步跳转地址
     */

    public static final String RETURN_URL = "";

    /**
     * 请求网关地址(线上和开发的不同)
     */
    public static final String GATEWAY_URL = "https://openapi.alipaydev.com/gateway.do";
    /**
     * 编码
     */
    public static final String CHARSET = "UTF-8";
    /**
     * 返回格式
     */
    public static final String FORMAT = "json";

    /**
     * RSA2
     */
    public static final String SIGN_TYPE = "RSA2";
}
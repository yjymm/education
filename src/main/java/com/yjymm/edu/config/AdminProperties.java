package com.yjymm.edu.config;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;

/**
 * @author yjymm
 * @date 2021-01-13 23:47
 */
@ConfigurationProperties(prefix = "admin")
@Data
public class AdminProperties {

    private String username;
    private String password;
}

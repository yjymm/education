package com.yjymm.edu.model.dto;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;

/**
 * @author yjymm
 * @date 2021-01-06 17:12
 */
@Data
public class OrderMapDTO implements Serializable {

    private static final long serialVersionUID = 80374969037945L;

    private Integer id;

    private Integer deleted;

    @ApiModelProperty(value = "此条记录所属用户")
    private Integer uid;

    @ApiModelProperty(value = "订单id")
    private Integer oid;

    @ApiModelProperty(value = "教师id，表示这个教师抢购这个订单，可以多个教师抢购同一个订单")
    private Integer tid;

    @ApiModelProperty(value = "0：抢订单，1：被家长选中，2：被家长拒绝")
    private Integer state;
}

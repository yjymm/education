package com.yjymm.edu.model.dto;

import lombok.Data;

import java.io.Serializable;
import java.util.List;

/**
 * @author yjymm
 * @date 2021-01-04 15:08
 */
@Data
public class UgtableDTO implements Serializable {

    private static final long serialVersionUID = 74237472L;


    private Integer id;

    private Integer deleted;

    private Integer uid;

    private Integer gid;

    private List<Integer> gids;
}

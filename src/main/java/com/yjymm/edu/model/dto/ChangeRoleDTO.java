package com.yjymm.edu.model.dto;

import lombok.Data;

/**
 * @author yjymm
 * @date 2021-05-04 23:35
 */
@Data
public class ChangeRoleDTO {
    private Integer id;
    private Integer roleId;
    
}

package com.yjymm.edu.model.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotNull;
import java.io.Serializable;

/**
 * @author yjymm
 * @date 2021-01-13 23:40
 */
@Data
public class AdminLoginDTO implements Serializable {

    private static final long serialVersionUID = 1L;

    @NotNull(message = "登陆用户名不能为空")
    @ApiModelProperty(value = "用户账号的用户名")
    private String username;

    @NotNull(message = "密码不能为空")
    @ApiModelProperty(value = "密码")
    private String password;

    private Long page;
    private Long size;
}

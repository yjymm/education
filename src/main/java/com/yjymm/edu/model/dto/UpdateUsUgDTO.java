package com.yjymm.edu.model.dto;

import lombok.Data;

import java.util.List;

/**
 * @author yjymm
 * @date 2021-04-06 23:01
 */
@Data
public class UpdateUsUgDTO {

    private List<Integer> gids;
    private List<Integer> sids;
    private Integer uid;
    private String openid;


}

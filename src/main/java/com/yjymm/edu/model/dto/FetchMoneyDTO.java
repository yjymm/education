package com.yjymm.edu.model.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDateTime;

/**
 * @author panhaoxiong
 * @date 2021-02-18 11:19
 */

@Data
public class FetchMoneyDTO implements Serializable {
    private static final long serialVersionUID = 70707971L;

    private Integer uid;
    private BigDecimal money;

    private LocalDateTime fetchTime = LocalDateTime.now();

}

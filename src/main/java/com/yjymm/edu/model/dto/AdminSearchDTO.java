package com.yjymm.edu.model.dto;

import lombok.Data;

/**
 * @author yjymm
 * @date 2021-04-21 9:56
 */
@Data
public class AdminSearchDTO {

    private Long page;
    private Long size;
    private String username;

}

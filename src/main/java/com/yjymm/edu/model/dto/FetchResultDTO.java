package com.yjymm.edu.model.dto;

import lombok.Data;

/**
 * @author panhaoxiong
 * @date 2021-03-26 15:08
 */

@Data
public class FetchResultDTO {
    private Integer fid;

    /**
     * 管理员审核意见
     * 0：提现不通过
     * 1：提现通过
     */
    private Integer adminOpCode = 1;
    
    private String result = "提现通过审核";

}

package com.yjymm.edu.model.dto;

import lombok.Data;

import java.io.Serializable;

/**
 * @author panhaoxiong
 * @date 2021-02-18 11:46
 */
@Data
public class OrderSearchDTO implements Serializable {
    private static final long serialVersionUID = 87076283641L;

    private Long page;
    private Long size;

//    private String ;


}

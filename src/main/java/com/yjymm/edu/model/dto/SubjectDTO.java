package com.yjymm.edu.model.dto;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotNull;
import java.io.Serializable;

/**
 * @author yjymm
 * @date 2021-01-03 22:14
 */

@Data
public class SubjectDTO implements Serializable{

    private static final long serialVersionUID = 2983426384L;

    private Integer id;

    private Integer deleted;

    @NotNull
    private String subjectName;

}

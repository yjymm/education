package com.yjymm.edu.model.dto;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.List;

/**
 * @author yjymm
 * @date 2021-01-06 10:51
 */
@Data
public class OrderDTO implements Serializable {
    private static final long serialVersionUID = 7237423470L;

    private Integer id;

    private Integer deleted;

    @ApiModelProperty(value = "对应user表的家长角色的id，哪个家长发起的订单")
    private Integer pid;

    @ApiModelProperty(value = "订单创建时间")
    private LocalDateTime createTime;

//    @JsonFormat(pattern = "yyyy-MM-dd")
//    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @ApiModelProperty(value = "订单业务开始时间")
    private LocalDateTime fromTime;

//    @JsonFormat(pattern = "yyyy-MM-dd")
//    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @ApiModelProperty(value = "订单业务结束时间")
    private LocalDateTime toTime;

    @ApiModelProperty(value = "订单总金额，家长会冻结相关金额，教师会收到相关金额")
    private BigDecimal money;

    private BigDecimal oldMoney;

    @ApiModelProperty(value = "订单是否完成")
    private Integer finished;

    @ApiModelProperty(value = "家长对于此订单的描述,要求")
    private String description;
    
    private Integer roleId;
    
    private String roleName;

    private List<Integer> gids;

    private List<Integer> sids;


    @ApiModelProperty(value = "国家")
    private String country;

    @ApiModelProperty(value = "省份")
    private String province;

    @ApiModelProperty(value = "城市")
    private String city;

    @ApiModelProperty(value = "区域")
    private String location;

    @ApiModelProperty(value = "详细地址")
    private String detailLocation;


}

package com.yjymm.edu.model.dto;

import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import java.io.Serializable;
import java.util.Date;

/**
 * @author panhaoxiong
 * @date 2021-02-06 14:05
 */
@Data
public class TimeSearchOrderDTO implements Serializable {
    private static final long serialVersionUID = 2343241L;

    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private Date from;
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private Date to;
    private Integer page;
    private Integer size;

//    private Integer uid;
//    private Boolean isParent;

}

package com.yjymm.edu.model.dto;

import lombok.Builder;
import lombok.Data;

/**
 * @author yjymm
 * @date 2021-04-19 10:26
 */
@Builder
@Data
public class FetchSearchDTO {

    private Long page;
    private Long size;
    private Integer uid;
    private Integer state;

    public FetchSearchDTO() {
    }

    public FetchSearchDTO(Long page, Long size, Integer uid, Integer state) {
        this.page = page;
        this.size = size;
        this.uid = uid;
        this.state = state;
    }
}

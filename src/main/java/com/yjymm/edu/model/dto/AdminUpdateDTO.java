package com.yjymm.edu.model.dto;

import lombok.Data;

/**
 * @author yjymm
 * @date 2021-05-04 20:08
 */
@Data
public class AdminUpdateDTO {

    private Integer id;
    private String username;
    private String password;
}

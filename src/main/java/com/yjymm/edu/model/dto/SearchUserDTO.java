package com.yjymm.edu.model.dto;

import lombok.Builder;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;

/**
 * @author yjymm
 * @date 2021-04-18 18:13
 */
@Data
@Builder
//@Getter
//@Setter
public class SearchUserDTO {
    
    private Integer roleId;
    private String username;
    private Long page;
    private Long size;

    public SearchUserDTO() {
    }

    public SearchUserDTO(Integer roleId, String username, Long page, Long size) {
        this.roleId = roleId;
        this.username = username;
        this.page = page;
        this.size = size;
    }
}

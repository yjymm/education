package com.yjymm.edu.model.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDateTime;

/**
 * @author yjymm
 * @date 2021-01-24 18:30
 */
@Data
public class RechargeDTO implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "创建充值订单时间")
    private LocalDateTime createTime = LocalDateTime.now();

    @NotNull
    @Min(1)
    private Integer uid;

    @NotNull
    @Min(value = 0)
    private BigDecimal money;
}

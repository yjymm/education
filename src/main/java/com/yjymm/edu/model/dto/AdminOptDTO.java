package com.yjymm.edu.model.dto;

import lombok.Data;

/**
 * @author yjymm
 * @date 2021-05-04 20:56
 */
@Data
public class AdminOptDTO {

    private Integer id;
    private Integer code;


}

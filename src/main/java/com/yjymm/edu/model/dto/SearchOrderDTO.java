package com.yjymm.edu.model.dto;

import com.yjymm.edu.model.common.OrderTimeEnum;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.Date;
import java.util.List;

/**
 * @author yjymm
 * @date 2021-01-06 14:09
 */

@Data
public class SearchOrderDTO implements Serializable {

    private static final long serialVersionUID = 77406507923L;

    private Long page = 1L;
    private Long size = 10L;
    private Integer uid;

    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private LocalDateTime from;
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private LocalDateTime to;

    private List<Integer> gids;
    private List<Integer> sids;

    // 订单状态
    @ApiModelProperty(value = "0:未完成(默认)，1：已取消，2：进行中，3：已完成")
    private Integer finished;

    /**
     * @see OrderTimeEnum
     */
    private Integer timeType;


}

package com.yjymm.edu.model.dto;

import lombok.Builder;
import lombok.Data;

import java.nio.channels.Pipe;

/**
 * @author yjymm
 * @date 2021-04-18 22:15
 */
@Data
@Builder
public class AdminLogDTO {

    private Long page;
    private Long size;
    private Integer type;
    private Integer state;


    public AdminLogDTO() {
    }


    public AdminLogDTO(Long page, Long size, Integer type, Integer state) {
        this.page = page;
        this.size = size;
        this.type = type;
        this.state = state;
    }
}

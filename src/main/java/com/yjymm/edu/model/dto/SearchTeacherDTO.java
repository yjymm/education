package com.yjymm.edu.model.dto;

import lombok.Data;
import org.hibernate.validator.constraints.Range;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.List;

/**
 * @author yjymm
 * @date 2021-01-04 11:28
 *
 * 搜索条件 DTO
 */
@Data
public class SearchTeacherDTO implements Serializable {
    
    private static final long serialVersionUID = 37459379457L;

    private List<Integer> gradeIds;
    private List<Integer> subjectIds;

    @NotNull
    @Min(value = 1, message = "从第一页开始")
    private Long page;

    @NotNull
    @Range(min = 10, max = 30, message = "每页最小10条记录，最大30条记录")
    private Long size;

    private String teacherName;

    private Integer uid;


}

package com.yjymm.edu.model.dto;

import lombok.Data;

import java.math.BigDecimal;

/**
 * @author yjymm
 * @date 2021-04-13 16:34
 */
@Data
public class TestDTO {

    private BigDecimal money;
}

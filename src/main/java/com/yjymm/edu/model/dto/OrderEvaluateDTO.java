package com.yjymm.edu.model.dto;

import lombok.Data;
import org.springframework.web.multipart.MultipartFile;

import java.io.Serializable;
import java.util.List;

/**
 * @author panhaoxiong
 * @date 2021-02-17 14:10
 */
@Data
public class OrderEvaluateDTO implements Serializable {
    
    private static final long serialVersionUID = 808971L;
    
    private Integer oid;
    private Integer score;
    private String description;
    private List<String> imgNames;
    private List<MultipartFile> imgFiles;

}

package com.yjymm.edu.model.dto;

import lombok.Data;

/**
 * @author yjymm
 * @date 2021-01-26 23:34
 */

@Data
public class PageDTO {

    private Long page = 1L;
    private Long size = 10L;

    public PageDTO(Long page, Long size) {
        this.page = page;
        this.size = size;
    }
}

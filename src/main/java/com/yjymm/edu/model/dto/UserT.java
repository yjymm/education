package com.yjymm.edu.model.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @author yjymm
 * @date 2020-12-27 0:20
 */
@Data
public class UserT {

    private Integer id;

    private Integer deleted;

    @ApiModelProperty(value = "微信小程序openId，唯一标识")
    private String openId;
    
    private String password;
    
}

package com.yjymm.edu.model.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.Range;
import org.springframework.lang.Nullable;
import org.springframework.validation.annotation.Validated;

import javax.validation.Valid;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Null;
import java.io.Serializable;

/**
 * @author yjymm
 * @date 2020-12-27 0:07
 */
@Data
public class UserDTO implements Serializable {

    private static final long serialVersionUID = 23926384692364L;

    @NotNull
    private Integer id;

    private Integer deleted;

    @ApiModelProperty(value = "微信小程序openId，唯一标识")
    private String openId;

    @ApiModelProperty(value = "1:男，2：女")
    private Integer gender;

    private String phone;

    @ApiModelProperty(value = "用户账号的用户名")
    private String username;

    @ApiModelProperty(value = "密码")
    private String password;

    @ApiModelProperty(value = "该用户的真实姓名")
    private String realName;

    @Email(message = "邮箱格式不正确")
    @Nullable
    private String email;

    @ApiModelProperty(value = "用户的角色")
    private Integer roleId;

    @ApiModelProperty(value = "用户账户余额，可以提现")
    @Range(min = 0L, message = "用户账户余额参数不正确")
    private Double balance;

    @ApiModelProperty(value = "用户冻结余额，不能体现")
    @Range(min = 0L, message = "用户冻结余额参数不正确")
    private Double freezeBalance;

    @ApiModelProperty(value = "用户头像")
    private String avatar;

    @ApiModelProperty(value = "国家")
    private String country;

    @ApiModelProperty(value = "省份")
    private String province;

    @ApiModelProperty(value = "城市")
    private String city;

    @ApiModelProperty(value = "个人描述")
    private String description;
}

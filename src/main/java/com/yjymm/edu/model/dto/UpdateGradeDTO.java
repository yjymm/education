package com.yjymm.edu.model.dto;

import lombok.Data;

/**
 * @author yjymm
 * @date 2021-04-20 16:44
 */
@Data
public class UpdateGradeDTO {
    private Integer id;
    private String gradeName;
}

package com.yjymm.edu.model.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import com.baomidou.mybatisplus.annotation.TableId;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 *
 * </p>
 *
 * @author yjymm
 * @since 2021-01-24
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@ApiModel(value="Recharge对象", description="")
public class Recharge extends Model<Recharge> {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    private Integer deleted;

    @ApiModelProperty(value = "创建充值订单时间")
    @JsonFormat(pattern="yyyy年MM月dd日 HH时mm分ss秒",timezone = "GMT+8")
    private LocalDateTime createTime;

    @ApiModelProperty(value = "充值时间")
    @JsonFormat(pattern="yyyy年MM月dd日 HH时mm分ss秒",timezone = "GMT+8")
    private LocalDateTime rechargeTime;

    @ApiModelProperty(value = "充值是否完成 0：未完成，1：已完成，2：已取消，3：已经超时")
    private Integer finished = 0;

    @ApiModelProperty(value = "完成时间")
    @JsonFormat(pattern="yyyy年MM月dd日 HH时mm分ss秒",timezone = "GMT+8")
    private LocalDateTime finishTime;

    private Integer uid;

    private BigDecimal money;

    @ApiModelProperty(value = "充值单号")
    private String rechargeNum;

    @Override
    protected Serializable pkVal() {
        return this.id;
    }

}

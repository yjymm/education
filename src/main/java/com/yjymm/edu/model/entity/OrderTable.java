package com.yjymm.edu.model.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import com.baomidou.mybatisplus.annotation.TableId;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.io.Serializable;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 *
 * </p>
 *
 * @author yjymm
 * @since 2021-02-18
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("order_table")
@ApiModel(value="OrderTable对象", description="")
public class OrderTable extends Model<OrderTable> {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    @ApiModelProperty(value = "订单号")
    private String orderNum;

    private Integer deleted;

    @ApiModelProperty(value = "对应user表的家长角色的id，哪个家长发起的订单")
    private Integer pid;

    @ApiModelProperty(value = "订单创建时间")
    private LocalDateTime createTime;

    @ApiModelProperty(value = "订单业务开始时间")
    private LocalDateTime fromTime;

    @ApiModelProperty(value = "订单业务结束时间")
    private LocalDateTime toTime;

    @ApiModelProperty(value = "订单总金额，家长会冻结相关金额，教师会收到相关金额")
    private BigDecimal money;

    @ApiModelProperty(value = "0:未完成(默认)，1：已取消，2：进行中，3：已完成")
    private Integer finished;

    @ApiModelProperty(value = "家长对于此订单的描述,要求")
    private String description;

    @ApiModelProperty(value = "此订单关联的年级，以 ;分割")
    private String gids;

    @ApiModelProperty(value = "此订单关联的科目，以 ;分割")
    private String sids;

    @ApiModelProperty(value = "用户账号的用户名, 冗余字段")
    private String username;

    @ApiModelProperty(value = "该用户的真实姓名,冗余字段")
    private String realName;


    @ApiModelProperty(value = "国家")
    private String country;

    @ApiModelProperty(value = "省份")
    private String province;

    @ApiModelProperty(value = "城市")
    private String city;

    @ApiModelProperty(value = "区域")
    private String location;

    @ApiModelProperty(value = "详细地址")
    private String detailLocation;



    @Override
    protected Serializable pkVal() {
        return this.id;
    }

}

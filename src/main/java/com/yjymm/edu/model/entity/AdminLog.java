package com.yjymm.edu.model.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * <p>
 * 
 * </p>
 *
 * @author yjymm
 * @since 2021-03-30
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("admin_log")
@ApiModel(value="AdminLog对象", description="")
public class AdminLog extends Model<AdminLog> {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    private Integer deleted;


    @ApiModelProperty(value = "消息创建时间")
    @JsonFormat(pattern="yyyy年MM月dd日",timezone = "GMT+8")
    private LocalDateTime createTime;

    @ApiModelProperty(value = "提现时间")
    private LocalDateTime logTime;

    @ApiModelProperty(value = "0：未读，1：已读")
    private Integer state;

    @ApiModelProperty(value = "消息描述")
    private String description;

    @ApiModelProperty(value = "消息类型 0：普通消息， 1：审核消息， 2：警告消息")
    private Integer type;

    @Override
    protected Serializable pkVal() {
        return this.id;
    }

}

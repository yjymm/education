package com.yjymm.edu.model.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import com.baomidou.mybatisplus.annotation.TableId;
import java.io.Serializable;
import java.math.BigDecimal;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.*;
import lombok.experimental.Accessors;

/**
 * <p>
 * 
 * </p>
 *
 * @author yjymm
 * @since 2020-12-25
 */
@Data
@Builder
@Getter
@Setter
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@ApiModel(value="User对象", description="")
public class User extends Model<User> {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    private Integer deleted;

    @ApiModelProperty(value = "微信小程序openId，唯一标识")
    private String openId;

    @ApiModelProperty(value = "0:男，1：女")
    private Integer gender;

    private String phone;

    @ApiModelProperty(value = "用户账号的用户名")
    private String username;

    @ApiModelProperty(value = "密码")
    private String password;

    @ApiModelProperty(value = "该用户的真实姓名")
    private String realName;

    private String email;

    @ApiModelProperty(value = "用户的角色")
    private Integer roleId;

    @ApiModelProperty(value = "用户账户余额，可以提现")
    private BigDecimal balance;

    @ApiModelProperty(value = "用户冻结余额，不能体现")
    private BigDecimal freezeBalance;

    @ApiModelProperty(value = "用户头像")
    private String avatar;

    @ApiModelProperty(value = "国家")
    private String country;

    @ApiModelProperty(value = "省份")
    private String province;

    @ApiModelProperty(value = "城市")
    private String city;

    @ApiModelProperty(value = "个人描述")
    private String description;

    @Override
    protected Serializable pkVal() {
        return this.id;
    }

    public User() {
    }

    public User(Integer id, Integer deleted, String openId, Integer gender, String phone, String username, String password, String realName, String email, Integer roleId, BigDecimal balance, BigDecimal freezeBalance, String avatar, String country, String province, String city, String description) {
        this.id = id;
        this.deleted = deleted;
        this.openId = openId;
        this.gender = gender;
        this.phone = phone;
        this.username = username;
        this.password = password;
        this.realName = realName;
        this.email = email;
        this.roleId = roleId;
        this.balance = balance;
        this.freezeBalance = freezeBalance;
        this.avatar = avatar;
        this.country = country;
        this.province = province;
        this.city = city;
        this.description = description;
    }
}

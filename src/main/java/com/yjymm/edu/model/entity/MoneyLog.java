package com.yjymm.edu.model.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDateTime;

/**
 * <p>
 * 
 * </p>
 *
 * @author yjymm
 * @since 2021-03-30
 */
@Data
@Builder
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("money_log")
@ApiModel(value="MoneyLog对象", description="")
public class MoneyLog extends Model<MoneyLog> {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    @ApiModelProperty(value = "用户id，消息属于哪个用户")
    private Integer uid;

    private Integer deleted;

    @ApiModelProperty(value = "用户账户当前余额：变更之后的余额")
    private BigDecimal balance;

    @ApiModelProperty(value = "用户账户当前冻结余额：变更之后的冻结余额")
    private BigDecimal freezeBalance;

    @ApiModelProperty(value = "用户变更的金额")
    private BigDecimal money;

    @ApiModelProperty(value = "消息创建时间")
    private LocalDateTime createTime;

    @ApiModelProperty(value = "0：未读，1：已读")
    private Integer state = 0;

    @ApiModelProperty(value = "消息描述")
    private String description;


    @Override
    protected Serializable pkVal() {
        return this.id;
    }

}

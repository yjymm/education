package com.yjymm.edu.model.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * <p>
 * 
 * </p>
 *
 * @author yjymm
 * @since 2021-01-30
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("order_evaluate_img")
@ApiModel(value="OrderEvaluateImg对象", description="")
public class OrderEvaluateImg extends Model<OrderEvaluateImg> {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    private Integer deleted;

    @ApiModelProperty(value = "评价id")
    private Integer eid;

    @ApiModelProperty(value = "评价图片")
    private String img;


    @Override
    protected Serializable pkVal() {
        return this.id;
    }

}

package com.yjymm.edu.model.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * <p>
 * 
 * </p>
 *
 * @author yjymm
 * @since 2021-03-30
 */

@Builder
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("user_log")
@ApiModel(value="UserLog对象", description="")
public class UserLog extends Model<UserLog> {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    @ApiModelProperty(value = "用户id，消息属于哪个用户")
    private Integer uid;

    private Integer deleted;

    @ApiModelProperty(value = "消息创建时间")
    @JsonFormat(pattern="yyyy年MM月dd日 HH时mm分ss秒",timezone = "GMT+8")
    private LocalDateTime createTime;

    @ApiModelProperty(value = "0：未读，1：已读")
    private Integer state;

    @ApiModelProperty(value = "消息描述")
    private String description;


    @Override
    protected Serializable pkVal() {
        return this.id;
    }

    public UserLog(Integer id, Integer uid, Integer deleted, LocalDateTime createTime, Integer state, String description) {
        this.id = id;
        this.uid = uid;
        this.deleted = deleted;
        this.createTime = createTime;
        this.state = state;
        this.description = description;
    }

    public UserLog() {
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getUid() {
        return uid;
    }

    public void setUid(Integer uid) {
        this.uid = uid;
    }

    public Integer getDeleted() {
        return deleted;
    }

    public void setDeleted(Integer deleted) {
        this.deleted = deleted;
    }

    public LocalDateTime getCreateTime() {
        return createTime;
    }

    public void setCreateTime(LocalDateTime createTime) {
        this.createTime = createTime;
    }

    public Integer getState() {
        return state;
    }

    public void setState(Integer state) {
        this.state = state;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}

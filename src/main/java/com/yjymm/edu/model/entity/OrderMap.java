package com.yjymm.edu.model.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import com.baomidou.mybatisplus.annotation.TableId;
import java.io.Serializable;
import java.time.LocalDateTime;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * @author yjymm
 * @since 2020-12-25
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("order_map")
@ApiModel(value="OrderMap对象", description="")
public class OrderMap extends Model<OrderMap> {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    private Integer deleted;

    @ApiModelProperty(value = "订单id")
    private Integer oid;

    @ApiModelProperty(value = "教师id，表示这个教师抢购这个订单，可以多个教师抢购同一个订单")
    private Integer tid;

    @ApiModelProperty(value = "0：抢订单，1：被家长选中，2：被家长拒绝")
    private Integer state;

    @ApiModelProperty(value = "被家长拒绝的原因")
    private String rejectReason;

    @ApiModelProperty(value = "订单抢占时间")
    private LocalDateTime createTime;


    @Override
    protected Serializable pkVal() {
        return this.id;
    }

}

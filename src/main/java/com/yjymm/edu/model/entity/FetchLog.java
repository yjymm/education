package com.yjymm.edu.model.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDateTime;

/**
 * <p>
 * 
 * </p>
 *
 * @author yjymm
 * @since 2021-03-30
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("fetch_log")
@ApiModel(value="FetchLog对象", description="")
public class FetchLog extends Model<FetchLog> {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    private Integer deleted;

    private Integer uid;

    @ApiModelProperty(value = "提现金额，不能小于等于0")
    private BigDecimal fetchMoney;

    @ApiModelProperty(value = "提现时间")
    @JsonFormat(pattern="yyyy年MM月dd日 HH时mm分ss秒",timezone = "GMT+8")
    private LocalDateTime fetchTime = LocalDateTime.now();

    @ApiModelProperty(value = "订单完成时间")
    @JsonFormat(pattern="yyyy年MM月dd日 HH时mm分ss秒",timezone = "GMT+8")
    private LocalDateTime finishTime;

    @ApiModelProperty(value = "提现状态（0：发起提现，提现审核中，1：提现成功，2：提现失败，3：用户取消提现）")
    private Integer state;

    @ApiModelProperty(value = "提现结果")
    private String result;

    @ApiModelProperty(value = "提现订单号")
    private String fetchNum;

    @Override
    protected Serializable pkVal() {
        return this.id;
    }

}

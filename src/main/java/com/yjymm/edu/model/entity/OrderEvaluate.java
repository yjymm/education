package com.yjymm.edu.model.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * <p>
 * 
 * </p>
 *
 * @author yjymm
 * @since 2021-01-30
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("order_evaluate")
@ApiModel(value="OrderEvaluate对象", description="")
public class OrderEvaluate extends Model<OrderEvaluate> {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    private Integer deleted;

    @ApiModelProperty(value = "订单号")
    private Integer oid;

    @ApiModelProperty(value = "订单评价星星")
    private Integer star;

    @ApiModelProperty(value = "评价描述")
    private String description;

    @ApiModelProperty(value = "订单评价图片，以;号分割")
    private String imgs;


    @Override
    protected Serializable pkVal() {
        return this.id;
    }

}

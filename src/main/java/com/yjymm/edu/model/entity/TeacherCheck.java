package com.yjymm.edu.model.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * <p>
 * 
 * </p>
 *
 * @author yjymm
 * @since 2021-04-20
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("teacher_check")
@ApiModel(value="TeacherCheck对象", description="")
public class TeacherCheck extends Model<TeacherCheck> {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    @ApiModelProperty(value = "申请订单号")
    private String checkNum;

    @ApiModelProperty(value = "用户id，消息属于哪个用户")
    private Integer uid;

    private Integer deleted;

    @ApiModelProperty(value = "消息创建时间")
    private LocalDateTime createTime;

    @ApiModelProperty(value = "0：申请，1：审核通过，2：拒绝通过")
    private Integer state;

    @ApiModelProperty(value = "审核结果说明")
    private String description;

    @ApiModelProperty(value = "额外属性，用作扩展")
    private String ext;

    @ApiModelProperty(value = "上传身份证照片信息")
    private String idCardImg;


    @Override
    protected Serializable pkVal() {
        return this.id;
    }

}

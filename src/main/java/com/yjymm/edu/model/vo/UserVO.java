package com.yjymm.edu.model.vo;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.yjymm.edu.model.entity.Role;
import com.yjymm.edu.model.entity.Ustable;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;

/**
 * @author yjymm
 * @date 2020-12-26 23:44
 */
@Data
public class UserVO implements Serializable {

    private static final long serialVersionUID = 123639613123L;

    private Integer id;

    private Integer deleted;

    @ApiModelProperty(value = "微信小程序openId，唯一标识")
    private String openId;

    @ApiModelProperty(value = "0:男，1：女")
    private Integer gender;

    private String phone;

    @ApiModelProperty(value = "用户账号的用户名")
    private String username;

    @ApiModelProperty(value = "密码")
    private String password;

    @ApiModelProperty(value = "该用户的真实姓名")
    private String realName;

    private String email;

    @ApiModelProperty(value = "用户的角色")
    private Integer roleId;

    @ApiModelProperty(value = "用户账户余额，可以提现")
    private BigDecimal balance;

    @ApiModelProperty(value = "用户冻结余额，不能体现")
    private BigDecimal freezeBalance;
    
    private String sessionKey;

    @ApiModelProperty(value = "国家")
    private String country;

    @ApiModelProperty(value = "省份")
    private String province;

    @ApiModelProperty(value = "城市")
    private String city;

    @ApiModelProperty(value = "用户头像")
    private String avatar;
    
    private Role role;

    private OrderVO orderVO;

    private List<UgtableVO> ugtableVOList;
    private List<UstableVO> ustableVOList;

    @ApiModelProperty(value = "个人描述")
    private String description;

    private List<String> gradeTitle;
    private List<String> subjectTitle;


    private Integer orderCount;
    private Integer rechargeCount;
    private Integer successRechargeC;
    private Integer failRechargeC;

    


}

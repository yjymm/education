package com.yjymm.edu.model.vo;

import lombok.*;

/**
 * @author yjymm
 * @date 2020-12-26 14:14
 */

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder(toBuilder = true)
@Getter
public class GradeVO {

    private Integer id;
    private String gradeName;
    private Boolean isChecked = Boolean.FALSE;

}

package com.yjymm.edu.model.vo;

import lombok.Data;

import java.io.Serializable;
import java.util.List;

/**
 * @author yjymm
 * @date 2021-01-04 23:37
 */

@Data
public class UstableVO implements Serializable {

    private static final long serialVersionUID = 74237472L;

    private Integer id;

    private Integer deleted;

    private Integer uid;

    private Integer sid;

    private List<Integer> sids;
}

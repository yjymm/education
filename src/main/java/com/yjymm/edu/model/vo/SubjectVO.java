package com.yjymm.edu.model.vo;

import lombok.Data;

import javax.validation.constraints.NotNull;
import java.io.Serializable;

/**
 * @author yjymm
 * @date 2021-01-03 23:11
 */
@Data
public class SubjectVO implements Serializable {

    private static final long serialVersionUID = 298388426384L;

    private Integer id;

    private Integer deleted;

    private String subjectName;

    private Boolean isChecked = Boolean.FALSE;

}

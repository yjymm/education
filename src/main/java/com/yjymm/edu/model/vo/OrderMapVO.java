package com.yjymm.edu.model.vo;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;

/**
 * @author yjymm
 * @date 2021-01-07 14:22
 */
@Data
public class OrderMapVO implements Serializable {
    
    private static final long serialVersionUID = 707097234L;

    private Integer id;

    private Integer deleted;

    @ApiModelProperty(value = "订单id")
    private Integer oid;

    @ApiModelProperty(value = "教师id，表示这个教师抢购这个订单，可以多个教师抢购同一个订单")
    private Integer tid;

    private String teacherName;
    private String teacherPhone;


    @ApiModelProperty(value = "0：抢订单，1：被家长选中，2：被家长拒绝")
    private Integer state;

    @ApiModelProperty(value = "被家长拒绝的原因")
    private String rejectReason;
}

package com.yjymm.edu.model.vo;

import lombok.Data;

import java.io.Serializable;

/**
 * @author yjymm
 * @date 2021-01-05 14:30
 */
@Data
public class UgtableVO implements Serializable {

    private static final long serialVersionUID = 7293749273894L;

    private Integer id;

    private Integer deleted;

    private Integer uid;

    private Integer gid;
}

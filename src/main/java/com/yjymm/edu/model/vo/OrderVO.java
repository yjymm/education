package com.yjymm.edu.model.vo;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.List;

/**
 * @author yjymm
 * @date 2021-01-06 11:48
 */
@Data
public class OrderVO implements Serializable {

    private static final long serialVersionUID = 273979927349L;

    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    private Integer deleted;

    @ApiModelProperty(value = "订单号")
    private String orderNum;

    @ApiModelProperty(value = "对应user表的家长角色的id，哪个家长发起的订单")
    private Integer pid;

    @ApiModelProperty(value = "订单创建时间")
    @JsonFormat(pattern="yyyy年MM月dd日",timezone = "GMT+8")
    private LocalDateTime createTime;

    @ApiModelProperty(value = "订单业务开始时间")
    @JsonFormat(pattern="yyyy年MM月dd日",timezone = "GMT+8")
    private LocalDateTime fromTime;

    @ApiModelProperty(value = "订单业务结束时间")
    @JsonFormat(pattern="yyyy年MM月dd日",timezone = "GMT+8")
    private LocalDateTime toTime;

    @ApiModelProperty(value = "订单总金额，家长会冻结相关金额，教师会收到相关金额")
    private BigDecimal money;

    @ApiModelProperty(value = "订单是否完成")
    private Integer finished;

    @ApiModelProperty(value = "家长对于此订单的描述,要求")
    private String description;

    @ApiModelProperty(value = "订单评价")
    private OrderEvaluateVO orderEvaluateVO;

    @ApiModelProperty(value = "用户账号的用户名, 冗余字段")
    private String username;

    @ApiModelProperty(value = "该用户的真实姓名,冗余字段")
    private String realName;
    
    private String avatar;


    @ApiModelProperty(value = "国家")
    private String country;

    @ApiModelProperty(value = "省份")
    private String province;

    @ApiModelProperty(value = "城市")
    private String city;

    @ApiModelProperty(value = "区域")
    private String location;

    @ApiModelProperty(value = "详细地址")
    private String detailLocation;

    private String workLocation;
    
    
    private String subjectTitle;
    private String gradeTitle;
    private String gids;
    private String sids;

    private List<OrderMapVO> orderMapVOList;
    private UserVO userVO;
    
    
    
    


}

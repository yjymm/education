package com.yjymm.edu.model.vo;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.yjymm.edu.model.entity.User;
import io.swagger.annotations.ApiModelProperty;
import lombok.Builder;
import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDateTime;

/**
 * @author yjymm
 * @date 2021-04-19 10:55
 */
@Data
@Builder
public class FetchVO implements Serializable {
    private static final long serialVersionUID = 1723472973L;

    private Integer id;

    private Integer deleted;

    private Integer uid;

    @ApiModelProperty(value = "提现金额，不能小于等于0")
    private BigDecimal fetchMoney;

    @ApiModelProperty(value = "提现时间")
    @JsonFormat(pattern="yyyy年MM月dd日 HH时mm分ss秒",timezone = "GMT+8")
    private LocalDateTime fetchTime = LocalDateTime.now();

    @ApiModelProperty(value = "订单完成时间")
    @JsonFormat(pattern="yyyy年MM月dd日 HH时mm分ss秒",timezone = "GMT+8")
    private LocalDateTime finishTime;

    @ApiModelProperty(value = "提现状态（0：发起提现，提现审核中，1：提现成功，2：提现失败，3：用户取消提现）")
    private Integer state;

    @ApiModelProperty(value = "提现结果")
    private String result;

    @ApiModelProperty(value = "提现订单号")
    private String fetchNum;

    private User user;

    public FetchVO() {
    }

    public FetchVO(Integer id, Integer deleted, Integer uid, BigDecimal fetchMoney, LocalDateTime fetchTime, LocalDateTime finishTime, Integer state, String result, String fetchNum, User user) {
        this.id = id;
        this.deleted = deleted;
        this.uid = uid;
        this.fetchMoney = fetchMoney;
        this.fetchTime = fetchTime;
        this.finishTime = finishTime;
        this.state = state;
        this.result = result;
        this.fetchNum = fetchNum;
        this.user = user;
    }
}

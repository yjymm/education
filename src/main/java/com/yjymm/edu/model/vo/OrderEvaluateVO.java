package com.yjymm.edu.model.vo;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.util.List;

/**
 * @author yjymm
 * @date 2021-01-30 15:42
 */
@Data
public class OrderEvaluateVO implements Serializable {
    private static final long serialVersionUID = 1L;

    private Integer id;

    private Integer deleted;

    @ApiModelProperty(value = "订单号")
    private Integer oid;

    @ApiModelProperty(value = "订单评价星星")
    private Integer star;

    @ApiModelProperty(value = "评价描述")
    private String description;

    @ApiModelProperty(value = "订单评价图片连接")
    private List<String> imgsUrl;

}

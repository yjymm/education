package com.yjymm.edu.model.vo;

import com.yjymm.edu.model.entity.User;
import lombok.Data;

/**
 * @author yjymm
 * @date 2021-01-17 21:07
 */
@Data
public class AdminLoginVo {
    private User user;
    private String token;
    private Integer expireMinutes;

    public AdminLoginVo(String token, Integer expireMinutes) {
        this.token = token;
        this.expireMinutes = expireMinutes;
    }

    public AdminLoginVo() {
    }
}

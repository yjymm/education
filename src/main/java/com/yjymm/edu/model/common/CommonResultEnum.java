package com.yjymm.edu.model.common;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.ToString;

/**
 * @author yjymm
 * @date 2020-12-26 0:10
 */
@JsonFormat(shape = JsonFormat.Shape.OBJECT)
@ToString
public enum  CommonResultEnum {
    // 2000 + 是成功的返回
    OK(2000,"成功"),

    // 4000 + 是拒绝，授权之类的返回
    NO_AUTHORIITY(4001,"没有权限"),
    NO_LOGIN(4002,"未登陆"),

    // 5000 + 是服务器错误的返回
    ERROR(5000,"服务器错误"),

    LOGIN_ERROR(5002,"微信登陆失败"),
    MYSQL_ERROR(5003, "数据库错误"),
    // bindingresult参数传递错误
    PARAMS_ERROR(5004, "参数传递错误"),

    // 6000 + 是 高度自定义的返回
    RECORD_EXISTS(6001, "该记录已存在"),
    CUSTOMER_PARAMS_ERROR(6002, "参数传递错误"),
    BALENCE_NOT_ENOUGH(6003, "余额不足"),
    MYERROR(6004,""),
    FILE_UPLOAD_ERROR(6005,""),
    TOKEN_EXPIRED(6666, "token过期，请重新登陆");

    private Integer code;
    private String message;

    private CommonResultEnum(Integer code, String message) {
        this.code = code;
        this.message = message;
    }

    public Integer getCode() {
        return code;
    }

    public String getMessage() {
        return message;
    }

}

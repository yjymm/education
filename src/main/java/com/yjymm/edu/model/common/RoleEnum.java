package com.yjymm.edu.model.common;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.ToString;

/**
 * @author yjymm
 * @date 2021-01-06 10:05
 */

@JsonFormat(shape = JsonFormat.Shape.OBJECT)
@ToString
public enum  RoleEnum {

    TEACHER(2,"ROLE_TEACHER", "家教教师"),
    ADMIN(1,"ROLE_ADMIN", "后台管理员"),
    USER(3,"ROLE_USER","普通用户"),
    PARENT(4,"ROLE_PARENT", "家长");

    private Integer id;

    private String roleName;

    private String description;

    RoleEnum(Integer id, String roleName, String description) {
        this.id = id;
        this.roleName = roleName;
        this.description = description;
    }

    public String getRoleName() {
        return roleName;
    }

    public String getDescription() {
        return description;
    }

    public Integer getId() {
        return id;
    }
}

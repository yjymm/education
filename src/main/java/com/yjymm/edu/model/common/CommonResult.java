package com.yjymm.edu.model.common;

import lombok.Builder;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;

import javax.naming.event.ObjectChangeListener;

/**
 * @author yjymm
 * @date 2020-12-26 0:08
 *
 * 统一返回对象
 */
@Builder(toBuilder = true)
@Data
public class CommonResult {

    private Object result;

    private Integer code;

    private String message;

    public void setInfo (CommonResultEnum resultEnum) {
        this.setCode(resultEnum.getCode());
        this.setMessage(resultEnum.getMessage());
    }

    public CommonResult() {
    }

    public CommonResult(Object result, Integer code, String message) {
        this.result = result;
        this.code = code;
        this.message = message;
    }

    public CommonResult(Object result, CommonResultEnum resultEnum) {
        this.result = result;
        this.setCode(resultEnum.getCode());
        this.setMessage(resultEnum.getMessage());
    }

    public static void main(String[] args) {
//        CommonResult.builder().
    }
}

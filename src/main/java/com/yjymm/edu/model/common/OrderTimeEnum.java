package com.yjymm.edu.model.common;

import org.springframework.core.annotation.Order;

import java.time.LocalDateTime;

/**
 * @author yjymm
 * @date 2021-04-01 23:55
 */
public enum OrderTimeEnum {
    THREE_DAYS(1, "近三天"){
        @Override
        public LocalDateTime getStartTime() {
            return LocalDateTime.now().minusDays(3);
        }
    },
    ONE_WEEK(2, "近一周") {
        @Override
        public LocalDateTime getStartTime() {
            return LocalDateTime.now().minusDays(7);
        }
    },
    THREE_WEEK(3, "近三周") {
        @Override
        public LocalDateTime getStartTime() {
            return LocalDateTime.now().minusDays(21);
        }
    },
    ONE_MONTH(4, "近一个月") {
        @Override
        public LocalDateTime getStartTime() {
            return LocalDateTime.now().minusDays(30);
        }
    },
    ;

    public Integer getId() {
        return id;
    }

    public String getDescription() {
        return description;
    }

    private Integer id;
    private String description;

    public static OrderTimeEnum getInstanceByType(Integer type) {
        OrderTimeEnum[] values = OrderTimeEnum.values();
        for (OrderTimeEnum timeEnum : values) {
            if (timeEnum.getId().equals(type)) {
                return timeEnum;
            }
        }
        return null;
    }

    public abstract LocalDateTime getStartTime();


    OrderTimeEnum(Integer id, String description) {
        this.id = id;
        this.description = description;
    }
}

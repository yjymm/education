package com.yjymm.edu.constant;

/**
 * @author yjymm
 * @date 2021-01-04 16:07
 */
public class UgtableConstant {

    // 存放每个用户感兴趣的 ugtable.interested.{uid}
    public static final String UG_USER_INTERESTED_KEY = "ugtable.interested.";
}

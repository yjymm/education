package com.yjymm.edu.constant;

import jdk.nashorn.internal.runtime.FindProperty;

/**
 * @author yjymm
 * @date 2021-01-06 11:53
 */
public class OrderConstant {

    public static final String ORDER_KEY = "order.id.";

    /**
     * 订单关联的年级 + oid
     */
    public static final String ORDER_WITH_GRADE_KEY = "order.gradeList.";

    /**
     * 订单关联的科目 + oid
     */
    public static final String ORDER_WITH_SUBJECT_KEY = "order.subjectList.";
}

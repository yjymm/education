package com.yjymm.edu.constant;

/**
 * @author yjymm
 * @date 2021-01-06 10:01
 */
public class RoleConstant {

    public static final String TEACHER_ROLE_NAME = "ROLE_TEACHER";
}

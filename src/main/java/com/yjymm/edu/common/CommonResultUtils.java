package com.yjymm.edu.common;

import com.sun.corba.se.impl.ior.OldJIDLObjectKeyTemplate;
import com.yjymm.edu.model.common.CommonResult;
import com.yjymm.edu.model.common.CommonResultEnum;

/**
 * @author yjymm
 * @date 2020-12-27 23:09
 */
public class CommonResultUtils {

    public static CommonResult build(Object object){
        CommonResult build = new CommonResult();
        build.setCode(CommonResultEnum.OK.getCode());
        build.setMessage(CommonResultEnum.OK.getMessage());
        build.setResult(object);
        return build;
    }
    public static CommonResult build(){
        CommonResult build = new CommonResult();
        build.setCode(CommonResultEnum.OK.getCode());
        build.setMessage(CommonResultEnum.OK.getMessage());
        build.setResult(null);
        return build;
    }
}

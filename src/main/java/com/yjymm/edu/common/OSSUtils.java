package com.yjymm.edu.common;

import com.yjymm.edu.config.OSSConfig;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;

/**
 * @author yjymm
 * @date 2021-01-21 23:46
 */
@Component
public class OSSUtils {

    @Resource
    private OSSConfig ossConfig;

    public String addUrlPrefix(String url){
        if (url == null) {
            return null;
        }
        if (!url.startsWith("http")) {
            return ossConfig.getUrlPrefix() + url;
        }
        return url;
    }

    public List<String> addUrlPrefixArray(String [] url){
        List<String> list = new ArrayList<>();
        for (int i = 0; i < url.length; i++) {
            if (url[i] == null) {
                continue;
            }
            if (!url[i].startsWith("http")) {
                list.add(ossConfig.getUrlPrefix() + url[i]);
            }
        }
        return list;
    }
}

package com.yjymm.edu.common.component;

import com.aliyun.oss.OSS;
import com.aliyun.oss.OSSClient;
import com.aliyun.oss.OSSClientBuilder;
import com.yjymm.edu.common.FileUtils;
import com.yjymm.edu.config.OSSConfig;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.io.FileInputStream;
import java.io.InputStream;

/**
 * @author panhaoxiong
 * @date 2021-02-17 11:20
 */

@Component
public class OSSComponent {

    @Resource
    private OSSClient ossClient;

    @Resource
    private OSSConfig ossConfig;

    /**
     * 上传文件，返回文件名（不带前缀）
     * @param inputStream
     * @return
     */
    public String uploadFile(InputStream inputStream, String fileName) throws Exception{
        ossClient.putObject(ossConfig.getBucketName(), fileName, inputStream);
        return fileName;
    }

    public String uploadFileWithRandonFileName(InputStream inputStream, String suffix) throws Exception{
        String fileName = FileUtils.randomFileName(suffix);
        return uploadFile(inputStream, fileName);
    }
}

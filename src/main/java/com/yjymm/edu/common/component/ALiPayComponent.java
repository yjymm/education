package com.yjymm.edu.common.component;

import com.alipay.api.DefaultAlipayClient;
import com.alipay.api.request.AlipayTradePagePayRequest;
import com.yjymm.edu.config.AlipayConfig;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Component;

/**
 * @author yjymm
 * @date 2021-04-09 0:20
 */

@Component
public class ALiPayComponent {

    @Bean
    public DefaultAlipayClient getClient() {
        return new DefaultAlipayClient(
                AlipayConfig.GATEWAY_URL, AlipayConfig.APP_ID, AlipayConfig.APP_PRIVATE_KEY,
                AlipayConfig.FORMAT, AlipayConfig.CHARSET, AlipayConfig.ALIPAY_PUBLIC_KEY,
                AlipayConfig.SIGN_TYPE);
    }

}

package com.yjymm.edu.common;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

/**
 * @author panhaoxiong
 * @date 2021-02-19 11:00
 */
public class StringUtils {

    /**
     *
     * @param source
     * @param separator
     * @param clazz  只能是包装类型
     * @param <T>  只能是包装类型
     * @return
     */
    public static  <T> List<T> getList(String source, String separator, Class<T> clazz) throws NoSuchMethodException, IllegalAccessException, InvocationTargetException, InstantiationException {
        List<T> list = null;
        if (source.endsWith(separator)) {
            source = source.substring(0, source.length() -1 );
        }
        String[] split = source.split(separator);
        list = new ArrayList<>(split.length);
        for (String s : split) {
            Constructor<T> declaredConstructor = clazz.getDeclaredConstructor(String.class);
            T t = declaredConstructor.newInstance(s);
            list.add(t);
        }
        return list;
    }

    public static Boolean isBlank(String s) {
        if (s == null) {
            return Boolean.TRUE;
        }
        if ("".equals(s)) {
            return Boolean.TRUE;
        }
        return Boolean.FALSE;
    }

    public static String getRandomString(String prefix) {
        String s = prefix + UUID.randomUUID().toString().replaceAll("-", "");
        if (s.length() > (20 + prefix.length())) {
            s = s.substring(0, 20 + prefix.length());
        }
        return s;
    }
}

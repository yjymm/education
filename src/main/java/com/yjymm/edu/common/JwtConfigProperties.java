package com.yjymm.edu.common;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;

/**
 * @author yjymm
 * @date 2021-01-14 23:50
 */

@ConfigurationProperties(prefix = "jwt.key")
@Data
public class JwtConfigProperties {

    private String publicKeyPath;
    private String privateKeyPath;
    private String secret;
    private String keySize;
    private Integer expireMinutes;
    private String adminTokenName;
    

    
    
}

package com.yjymm.edu.common;

import java.util.UUID;

/**
 * @author yjymm
 * @date 2021-01-07 22:56
 */
public class FileUploadUtils {

    /**
     * 生成随机文件名称
     * @param suffix  文件后缀,不需要携带 "."
     * @return
     */
    public static String randomFileName(String suffix) {
        return UUID.randomUUID().toString().replaceAll("-","") + "." + suffix;
    }
}

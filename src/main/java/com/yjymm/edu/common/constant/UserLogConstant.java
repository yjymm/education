package com.yjymm.edu.common.constant;

/**
 * @author yjymm
 * @date 2021-03-30 21:48
 */
public class UserLogConstant {

    public static String USER_FETCH_FETCHLOG_NOTICE = "您于时间【%s】申请账户提现，提现金额为【%.2f】，请耐心" +
            "等待管理员审核";

    public static String DO_RECHARGE = "账户充值";
    public static String USER_CANCEL_FETCH_NOTICE = "您于时间【%s】取消订单：【%d】的账户提现，金额为【%.2f】，请留意账户余额";

    public static String ADMIN_REJECT_USER_FETCH = "提现审核不通过，有任何疑问可以询问管理员";
    public static String ADMIN_ALLOW_USER_FETCH = "提现审核通过，请及时检查您的账户余额";

    /**
     * 用户发布订单
     */
    public static String USER_DISPATCH_ORDER = "您于时间【%s】发布订单，订单号【%s】";
    public static String USER_UPDATE_ORDER = "";

    public static String USER_ALIPAY = "您于时间【%s】进行账户充值，充值金额为【%.2f】，充值订单号【%s】";

}




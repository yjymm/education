package com.yjymm.edu.common.constant;

/**
 * @author yjymm
 * @date 2021-03-30 21:39
 */
public class AdminLogConstant {

    public static String USER_FETCH_NOTICE_ADMIN = "用户【%s】，ID【%d】，真实名称：【%s】，" +
            "于时间：【%s】申请账户提现，提现金额：【%.2f】，请及时审核";

    public static String USER_CANCEL_FETCH_NOTICE = "用户【%s】，ID【%d】，" +
            "于时间：【%s】取消账户提现，提现订单id：【%d】，取消提现金额：【%.2f】，请注意";

    public static String USER_DISPATCH_ORDER_NOTICE = "用户【%s】，ID【%d】，" +
            "于时间：【%s】发布了家教订单，订单id：【%d】";
}

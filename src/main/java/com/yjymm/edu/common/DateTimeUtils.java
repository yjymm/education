package com.yjymm.edu.common;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

/**
 * @author panhaoxiong
 * @date 2021-02-18 14:34
 */
public class DateTimeUtils {

    public static String FORMAT_PATTREN_ONE = "yyyy年MM月dd日 HH:mm:ss";


    public static String formatStr (LocalDateTime date, String pattern) {
        return DateTimeFormatter.ofPattern(pattern).format(date);
    }

    public static String formatStr (LocalDateTime date) {
        return DateTimeFormatter.ofPattern(DateTimeUtils.FORMAT_PATTREN_ONE).format(date);
    }
}

package com.yjymm.edu.common;

import java.util.UUID;

/**
 * @author panhaoxiong
 * @date 2021-02-07 15:06
 */
public class FileUtils {

    /**
     * 生成随机文件名称 eg:jpg
     * @param suffix  文件后缀,不需要携带 "."
     * @return
     */
    public static String randomFileName(String suffix) {
        return UUID.randomUUID().toString().replaceAll("-","") + "." + suffix;
    }
}

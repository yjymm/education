package com.yjymm.edu.controller;


import com.yjymm.edu.common.CommonResultUtils;
import com.yjymm.edu.model.common.CommonResult;
import com.yjymm.edu.model.dto.OrderMapDTO;
import com.yjymm.edu.model.entity.OrderMap;
import com.yjymm.edu.model.vo.OrderMapVO;
import com.yjymm.edu.service.OrderMapService;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.sql.SQLException;
import java.util.List;

/**
 *
 * @author yjymm
 * @since 2020-12-25
 */
@RestController
@RequestMapping("/orderMap")
public class OrderMapController {

    @Resource
    private OrderMapService orderMapService;

    @PostMapping("/insert")
    public CommonResult insert(@RequestBody OrderMapDTO orderMapDTO) throws Exception {
        orderMapService.insert(orderMapDTO);
        return CommonResultUtils.build(null);
    }

    @PostMapping("/update")
    public CommonResult update(@RequestBody OrderMapDTO orderMapDTO) throws SQLException {
        orderMapService.updateOrderMap(orderMapDTO);
        return CommonResultUtils.build(null);
    }

    /**
     * 教师抢订单
     * @param oid
     * @param tid
     * @return
     */
    @GetMapping("/receiveOrder/{oid}/{tid}")
    public CommonResult robOrder(@PathVariable("oid") Integer oid,
                                     @PathVariable("tid") Integer tid) throws SQLException {
        orderMapService.robOrder(oid, tid);
        return CommonResultUtils.build(null);
    }

    /**
     * 用户选择某个教师作为订单的承接人
     * @param orderMapDTO
     * @return
     * @throws SQLException
     */
    @PostMapping("/chooseTeacher")
    public CommonResult chooseTeacher(@RequestBody OrderMapDTO orderMapDTO) throws Exception {
        orderMapService.chooseTeacher(orderMapDTO);
        return CommonResultUtils.build(null);
    }


    @GetMapping("/listByOid")
    public CommonResult listByOid(@RequestBody OrderMapDTO orderMapDTO) throws SQLException {
        List<OrderMapVO> orderMapVOS =  orderMapService.listByOid(orderMapDTO);
        return CommonResultUtils.build(orderMapVOS);
    }
}

package com.yjymm.edu.controller;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.yjymm.edu.common.CommonResultUtils;
import com.yjymm.edu.model.common.CommonResult;
import com.yjymm.edu.model.dto.SearchOrderDTO;
import com.yjymm.edu.model.dto.SearchTeacherDTO;
import com.yjymm.edu.model.dto.SearchUserDTO;
import com.yjymm.edu.service.SearchService;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;

/**
 * @author yjymm
 * @date 2021-01-05 17:24
 */

@RestController
@RequestMapping("/search")
public class SearchController {

    @Resource
    private SearchService searchService;

    /**
     * 对家教老师进行搜索
     *
     * @return
     */
    @PostMapping("/teacher")
    public CommonResult search(@Validated @RequestBody SearchTeacherDTO searchTeacherDTO) {
        IPage page = searchService.search(searchTeacherDTO);
        return CommonResultUtils.build(page);
    }

    /**
     * 根据家长用户设置的感兴趣的标签，返回对应的教师信息
     *
     * @param searchTeacherDTO
     * @return
     */
    @PostMapping("/getFarvoriteTeacher")
    public CommonResult getFarvoriteTeacher(@RequestBody SearchTeacherDTO searchTeacherDTO) {
        IPage page = searchService.getFarvoriteTeacher(searchTeacherDTO);
        return CommonResultUtils.build(page);
    }

    @PostMapping("/user")
    public CommonResult searchUser(@RequestBody SearchUserDTO dto) {
        IPage page = searchService.searchUser(dto);
        return CommonResultUtils.build(page);
    }


}

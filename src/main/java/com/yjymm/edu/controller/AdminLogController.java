package com.yjymm.edu.controller;


import com.baomidou.mybatisplus.core.metadata.IPage;
import com.yjymm.edu.common.CommonResultUtils;
import com.yjymm.edu.model.common.CommonResult;
import com.yjymm.edu.model.dto.AdminLogDTO;
import com.yjymm.edu.service.AdminLogService;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;

/**
 * 管理员通知
 *
 * @author yjymm
 * @since 2021-02-18
 */
@RestController
@RequestMapping("/adminLog")
public class AdminLogController {

    @Resource
    private AdminLogService adminLogService;

    @PostMapping("/list")
    public CommonResult list(@RequestBody AdminLogDTO dto) {
        IPage page = adminLogService.getList(dto);
        return CommonResultUtils.build(page);
    }

    @GetMapping("/read/{lid}")
    public CommonResult read(@PathVariable("lid") Integer lid){
        adminLogService.read(lid);
        return CommonResultUtils.build(null);
    }
}

package com.yjymm.edu.controller;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.yjymm.edu.common.CommonResultUtils;
import com.yjymm.edu.model.common.CommonResult;
import com.yjymm.edu.model.dto.AdminLoginDTO;
import com.yjymm.edu.model.dto.AdminOptDTO;
import com.yjymm.edu.model.dto.AdminSearchDTO;
import com.yjymm.edu.model.dto.AdminUpdateDTO;
import com.yjymm.edu.model.entity.FetchLog;
import com.yjymm.edu.model.vo.AdminLoginVo;
import com.yjymm.edu.service.AdminService;
import com.yjymm.edu.service.impl.AdminServiceImpl;
import org.apache.http.conn.util.PublicSuffixList;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.validation.Valid;

/**
 * @author yjymm
 * @date 2021-03-21 1:58
 * 管理员操作接口
 */
@RestController
@RequestMapping("/admin")
public class AdminController {

    @Resource
    private AdminService adminServiceImpl;

    /**
     * 管理员登陆
     * @param dto
     * @return
     * @throws Exception
     */
    @PostMapping("/login")
    public CommonResult adminLogin(@Valid @RequestBody AdminLoginDTO dto) throws Exception {
        AdminLoginVo adminLoginVo = adminServiceImpl.adminLogin(dto);
        return CommonResultUtils.build(adminLoginVo);
    }

    @PostMapping("/insert")
    public CommonResult insert(@RequestBody AdminLoginDTO dto) throws Exception {
        adminServiceImpl.insertAdmin(dto);
        return CommonResultUtils.build(null);
    }

    @PostMapping("/list")
    public CommonResult list(@RequestBody AdminSearchDTO dto) throws Exception {
        IPage result = adminServiceImpl.list(dto);
        return CommonResultUtils.build(result);
    }

    @PostMapping("/updatePass")
    public CommonResult list(@RequestBody AdminUpdateDTO dto) throws Exception {
        adminServiceImpl.updatePass(dto);
        return CommonResultUtils.build(null);
    }

    @PostMapping("/statusOpt")
    public CommonResult statusOpt(@RequestBody AdminOptDTO dto) throws Exception {
        adminServiceImpl.statusOpt(dto.getId(), dto.getCode());
        return CommonResultUtils.build(null);
    }
}

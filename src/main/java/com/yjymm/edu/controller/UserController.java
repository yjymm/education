package com.yjymm.edu.controller;


import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.yjymm.edu.common.CommonResultUtils;
import com.yjymm.edu.model.common.CommonResult;
import com.yjymm.edu.model.common.RoleEnum;
import com.yjymm.edu.model.dto.*;
import com.yjymm.edu.model.entity.Ugtable;
import com.yjymm.edu.model.entity.User;
import com.yjymm.edu.model.vo.AdminLoginVo;
import com.yjymm.edu.model.vo.OrderVO;
import com.yjymm.edu.model.vo.UserVO;
import com.yjymm.edu.service.UserService;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.Resource;
import javax.validation.Valid;
import java.io.IOException;
import java.sql.SQLException;
import java.util.List;

/**
 *
 * @author yjymm
 * @since 2020-12-25
 */
@RestController
@RequestMapping("/user")
public class UserController {

    @Resource
    private UserService userService;


    @PostMapping("/insert")
    public CommonResult insert(@RequestBody @Validated UserDTO userDTO) throws SQLException {
        userService.insert(userDTO);
        return CommonResultUtils.build(null);
    }

    /**
     * 批量伪删除用户
     * @param uids
     * @return
     * @throws SQLException
     */
    @PostMapping("/delete")
    public CommonResult insert(@RequestBody List<Integer> uids) throws SQLException {
        userService.delete(uids);
        return null;
    }

    @PostMapping("/update")
    public CommonResult update(@RequestBody @Validated UserDTO userDTO) throws SQLException {
        UserVO userVO = userService.updateUser(userDTO);
        return CommonResultUtils.build(userVO);
    }

    /**
     * 用户上传头像
     */
    @PostMapping("/uploadAvatar/{id}")
    public CommonResult uploadAvatar(@PathVariable("id") Integer id,
                                     @RequestParam("file") MultipartFile file) throws IOException, SQLException {
        userService.uploadAvatar(id, file);
        return null;
    }

    @GetMapping("/getById/{id}")
    public CommonResult getById(@PathVariable("id") Integer id) throws Exception {
        UserVO user = userService.getDetailById(id);
        return CommonResultUtils.build(user);
    }

    @GetMapping("/getByOpenId/{openId}")
    public CommonResult getByOpenId(@PathVariable("openId") String openId) throws Exception {
        UserVO userVO = userService.getByOpenId(openId);
        return CommonResultUtils.build(userVO);
    }


    /**
     * 所用用户分页展示
     * @param size
     * @param page
     * @return
     */
    @GetMapping("/listPage/{size}/{page}")
    public CommonResult listPage(
            @PathVariable("size") Long size,
            @PathVariable("page") Long page
    ){
        IPage<UserVO> page1 = userService.listPageWithWrapper(size,page, null);

        return CommonResultUtils.build(page1);
    }


    /**
     * 所用教师用户分页展示
     * @param size
     * @param page
     * @return
     */
    @GetMapping("/listTeacherPage/{size}/{page}")
    public CommonResult listTeacherPage(
            @PathVariable("size") Long size,
            @PathVariable("page") Long page
    ){
        QueryWrapper<User> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("role_id", RoleEnum.TEACHER.getId());
        IPage<UserVO> page1 = userService.listPageWithWrapper(size,page, queryWrapper);
        return CommonResultUtils.build(page1);
    }

    /**
     * 所用家长用户分页展示
     * @param size
     * @param page
     * @return
     */
    @GetMapping("/listParentPage/{size}/{page}")
    public CommonResult listParentPage(
            @PathVariable("size") Long size,
            @PathVariable("page") Long page
    ){
        QueryWrapper<User> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("role_id", RoleEnum.PARENT.getId());
        IPage<UserVO> page1 = userService.listPageWithWrapper(size,page, queryWrapper);
        return CommonResultUtils.build(page1);
    }


    @PostMapping("/updateUsAndUg")
    public CommonResult updateUsAndUg(@RequestBody UpdateUsUgDTO dto) throws SQLException {
        userService.updateUsAndUg(dto);
        return CommonResultUtils.build(null);
    }

    @GetMapping("/getTeacherDetaiById/{tid}")
    public CommonResult getTeacherDetaiById(@PathVariable("tid") Integer tid) throws Exception {
        UserVO userVO = userService.getTeacherDetaiById(tid);
        return CommonResultUtils.build(userVO);
    }

    @GetMapping("/count")
    public CommonResult getCount(){
        Integer i = userService.getCount();
        return CommonResultUtils.build(i);
    }

    @PostMapping("/changeRole")
    public CommonResult changeRole(@RequestBody ChangeRoleDTO dto){
        userService.changeRole(dto);
        return CommonResultUtils.build();
    }

}

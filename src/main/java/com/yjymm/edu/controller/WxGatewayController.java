package com.yjymm.edu.controller;

import com.yjymm.edu.common.CommonResultUtils;
import com.yjymm.edu.model.common.CommonResult;
import com.yjymm.edu.model.vo.UserVO;
import com.yjymm.edu.service.UserService;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.HashMap;

/**
 * @author yjymm
 * @date 2020-12-26 22:55
 */

@RestController
@RequestMapping("/wx_gateway")
public class WxGatewayController {

    @Resource
    private UserService userService;

    @GetMapping("/openId/{code}")
    public CommonResult getOpenId(@PathVariable("code") String code) throws Exception {
        UserVO userVO = userService.getOpenId(code);
        return CommonResultUtils.build(userVO);
    }
}

package com.yjymm.edu.controller;


import com.yjymm.edu.common.CommonResultUtils;
import com.yjymm.edu.common.MyBeanUtils;
import com.yjymm.edu.config.WxConfigProperty;
import com.yjymm.edu.model.common.CommonResult;
import com.yjymm.edu.model.dto.UpdateGradeDTO;
import com.yjymm.edu.model.entity.Grade;
import com.yjymm.edu.model.vo.GradeVO;
import com.yjymm.edu.service.GradeService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.sql.SQLException;
import java.util.List;

/**
 *  年级controller
 * @author yjymm
 * @since 2020-12-25
 */
@Slf4j
@RestController
@RequestMapping("/grade")
public class GradeController {

    @Resource
    private GradeService gradeService;

    /**
     * 获取所有年级列表
     * @return
     */
    @GetMapping(value = "/list", produces = MediaType.APPLICATION_JSON_VALUE)
    public CommonResult list(){
        List<Grade> list = gradeService.list(null);
        List<GradeVO> gradeVOS = MyBeanUtils.converToList(list, GradeVO.class);
        return CommonResultUtils.build(gradeVOS);
    }

    /**
     * 查询所有年级，用户感兴趣的年级就 ischecked = true
     * @param openId
     * @return
     */
    @GetMapping(value = "/listUserFavoriteByOpenId/{openId}", produces = MediaType.APPLICATION_JSON_VALUE)
    public CommonResult listUserFavoriteByOpenId(@PathVariable("openId") String openId){
        List<GradeVO> gradeVOS = gradeService.listUserFavoriteByOpenId(openId);
        return CommonResultUtils.build(gradeVOS);
    }


    @GetMapping(value = "/getBId/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public CommonResult getById(@PathVariable("id") Integer id){
        Grade byId = gradeService.getById(id);
        return CommonResultUtils.build(byId);
    }

    @PostMapping("/insert")
    public CommonResult insert(@RequestBody Grade grade) throws Exception {
        gradeService.insertGrade(grade);
        return CommonResultUtils.build(null);
    }

    @PostMapping("/delete/")
    public CommonResult delete(@RequestBody List<Integer> gids) throws SQLException {
        gradeService.deleteBatchById(gids);
        return CommonResultUtils.build(null);
    }

    @PostMapping("/update")
    public CommonResult update(@RequestBody Grade grade) {

        gradeService.updateGrade(grade);
        return CommonResultUtils.build(null);
    }

}

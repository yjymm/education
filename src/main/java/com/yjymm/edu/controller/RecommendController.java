package com.yjymm.edu.controller;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.yjymm.edu.common.CommonResultUtils;
import com.yjymm.edu.model.common.CommonResult;
import com.yjymm.edu.service.RecommendService;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

/**
 * @author yjymm
 * @date 2021-03-30 23:59
 *
 * 用户推荐
 */

@RestController
@RequestMapping("/recom")
public class RecommendController {

    @Resource
    private RecommendService recommendService;

    /**
     * 根据用户推荐感兴趣的教师
     * @return
     */
    @GetMapping("/teacher/{uid}")
    public CommonResult recommendedTeacher(@PathVariable("uid") Integer uid) {
        IPage page = recommendService.recommendedTeacher(uid);
        return CommonResultUtils.build(page);
    }
}

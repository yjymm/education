package com.yjymm.edu.controller;


import com.baomidou.mybatisplus.core.metadata.IPage;
import com.yjymm.edu.common.CommonResultUtils;
import com.yjymm.edu.model.common.CommonResult;
import com.yjymm.edu.model.dto.FetchMoneyDTO;
import com.yjymm.edu.model.dto.FetchResultDTO;
import com.yjymm.edu.model.dto.FetchSearchDTO;
import com.yjymm.edu.model.entity.FetchLog;
import com.yjymm.edu.service.FetchLogService;
import org.apache.http.conn.util.PublicSuffixList;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.sql.SQLException;

/**
 * 用户提现
 * @author yjymm
 * @since 2021-03-21
 */
@RestController
@RequestMapping("/fetchLog")
public class FetchLogController {

    @Resource
    private FetchLogService fetchLogService;

    /**
     * 用户发起提现
     */
    @PostMapping("/insert")
    public CommonResult insert(@RequestBody FetchMoneyDTO fetchMoneyDTO) throws SQLException {
        fetchLogService.insertFetch(fetchMoneyDTO);
        return CommonResultUtils.build(null);
    }

    /**
     * 用户取消提现
     * @param fid
     * @param uid
     * @return
     */
    @GetMapping("/cancel/{fid}/{uid}")
    public CommonResult cancel(@PathVariable("fid") Integer fid,
                               @PathVariable("uid") Integer uid){
        fetchLogService.cancel(fid, uid);
        return CommonResultUtils.build(null);
    }

    /**
     * 获取用户所有提现订单
     * @param uid
     * @return
     */
    @GetMapping("/getList/{uid}")
    public CommonResult getList(@PathVariable("uid") Integer uid){
        return CommonResultUtils.build(fetchLogService.getList(uid));
    }

    /**
     * 管理员审核用户提现订单
     */
    @PostMapping("/checkFecthLog")
    public CommonResult checkFecthLog(@RequestBody FetchResultDTO fetchResultDTO) {
        fetchLogService.checkFecthLog(fetchResultDTO);
        return CommonResultUtils.build(null);
    }

    @PostMapping("/list")
    public CommonResult list(@RequestBody FetchSearchDTO dto){
        IPage result = fetchLogService.searchList(dto);
        return CommonResultUtils.build(result);
    }

    @GetMapping("/count")
    public CommonResult getCount() throws Exception {
        return CommonResultUtils.build(fetchLogService.getCount());
    }
}

package com.yjymm.edu.controller;


import com.yjymm.edu.common.CommonResultUtils;
import com.yjymm.edu.common.MyBeanUtils;
import com.yjymm.edu.model.common.CommonResult;
import com.yjymm.edu.model.dto.SubjectDTO;
import com.yjymm.edu.model.entity.Grade;
import com.yjymm.edu.model.entity.Subject;
import com.yjymm.edu.model.vo.SubjectVO;
import com.yjymm.edu.service.SubjectService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.http.MediaType;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.sql.SQLException;
import java.util.List;

/**
 * 科目
 * @author yjymm
 * @since 2020-12-25
 */
@RestController
@RequestMapping("/subject")
@Slf4j
public class SubjectController {

    @Resource
    private SubjectService subjectService;

    @GetMapping(value = "/list", produces = MediaType.APPLICATION_JSON_VALUE)
    public CommonResult list(){
        List<Subject> list = subjectService.list(null);
        List<SubjectVO> subjectVOS = MyBeanUtils.converToList(list, SubjectVO.class);
        return CommonResultUtils.build(subjectVOS);
    }

    /**
     * 查询所有科目，用户感兴趣的科目就ischecked = true
     * @param openId
     * @return
     */
    @GetMapping(value = "/listUserFavoriteSubjectByOpenId/{openId}", produces = MediaType.APPLICATION_JSON_VALUE)
    public CommonResult listUserFavoriteSubjectByOpenId(@PathVariable("openId") String openId){
        List<SubjectVO> subjectVOS = subjectService.listUserFavoriteSubjectByOpenId(openId);
        return CommonResultUtils.build(subjectVOS);
    }

    @PostMapping(value = "/insert" , produces = MediaType.APPLICATION_JSON_VALUE)
    public CommonResult insert( @RequestBody Subject subject) throws Exception {
        subjectService.insertSubject(subject);
        return CommonResultUtils.build(null);
    }

    @PostMapping("/update")
    public CommonResult update(@RequestBody Subject subject) {
        subjectService.updateById(subject);
        return CommonResultUtils.build(null);
    }

}

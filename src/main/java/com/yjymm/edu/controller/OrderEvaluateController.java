package com.yjymm.edu.controller;


import com.yjymm.edu.model.common.CommonResult;
import com.yjymm.edu.model.dto.OrderEvaluateDTO;
import com.yjymm.edu.service.OrderEvaluateService;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.Resource;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author yjymm
 * @since 2021-01-30
 */
@RestController
@RequestMapping("/orderEvaluate")
public class OrderEvaluateController {

    @Resource
    private OrderEvaluateService orderEvaluateService;

    @PostMapping("/uploadImg")
    public CommonResult uploadImg(@RequestParam("imgFile") MultipartFile multipartFile) throws Exception {
        return orderEvaluateService.uploadImg(multipartFile);
    }

    /**
     * 新增订单评价
     * @param dto
     * @return
     */
    @PostMapping("insert")
    public CommonResult insert(@RequestBody OrderEvaluateDTO dto) throws Exception {
        orderEvaluateService.insert(dto);
        return null;
    }

}

package com.yjymm.edu.controller;


import com.baomidou.mybatisplus.core.metadata.IPage;
import com.yjymm.edu.common.CommonResultUtils;
import com.yjymm.edu.common.MyBeanUtils;
import com.yjymm.edu.model.common.CommonResult;
import com.yjymm.edu.model.dto.RechargeDTO;
import com.yjymm.edu.model.entity.Recharge;
import com.yjymm.edu.service.RechargeService;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.validation.Valid;
import java.util.List;

/**
 *
 * 用户账户充值
 * @author yjymm
 * @since 2020-12-25
 */
@RestController
@RequestMapping("/recharge")
public class RechargeController {

    @Resource
    private RechargeService rechargeService;

    /**
     * 创建用户充值订单
     * @param rechargeDTO
     * @return
     */
    @PostMapping("/insert")
    public CommonResult insert(@Valid @RequestBody RechargeDTO rechargeDTO) throws Exception{
        Recharge insert = rechargeService.insert(rechargeDTO);
        return CommonResultUtils.build(insert);
    }

    /**
     * 用户确定充值或取消充值
     * @param rid
     * @param uid
     * @param recharge 1:充值，0取消
     * @return
     * @throws Exception
     */
    @PostMapping("/doRecharge/{rid}/{uid}/{recharge}")
    public CommonResult doRecharge(@PathVariable("rid") Integer rid,
                                   @PathVariable("uid") Integer uid,
                                   @PathVariable("recharge") Integer recharge) throws Exception{
        rechargeService.doRecharge(rid, uid, recharge);
        return null;
    }

    /**
     * 获取用户充值订单
     * @param uid
     * @return
     */
    @GetMapping("/getListByUid/{uid}")
    public CommonResult getListByUid(@PathVariable("uid") Integer uid) {
        List<Recharge> result = rechargeService.getListByUid(uid);
        return CommonResultUtils.build(result);
    }

    @GetMapping("/count")
    public CommonResult getCount() throws Exception {
        return CommonResultUtils.build(rechargeService.getCount());
    }
}

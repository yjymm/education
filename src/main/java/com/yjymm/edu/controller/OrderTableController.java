package com.yjymm.edu.controller;


import com.baomidou.mybatisplus.core.metadata.IPage;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.yjymm.edu.common.CommonResultUtils;
import com.yjymm.edu.model.common.CommonResult;
import com.yjymm.edu.model.dto.*;
import com.yjymm.edu.model.entity.OrderTable;
import com.yjymm.edu.model.vo.OrderVO;
import com.yjymm.edu.service.OrderTableService;
import com.yjymm.edu.service.UserService;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.sql.SQLException;
import java.util.List;

/**
 *
 * @author yjymm
 * @since 2020-12-25
 */
@RestController
@RequestMapping("/order")
public class OrderTableController {

    @Resource
    private UserService userService;

    @Resource
    private OrderTableService orderService;

    /**
     * 家长发布订单
     * @return
     */
    @PostMapping("/dispatchOrder")
    public CommonResult dispatchOrder(@RequestBody OrderDTO orderDTO) throws SQLException {
        orderService.dispatchOrder(orderDTO);
        return CommonResultUtils.build(null);
    }


    @PostMapping("/update")
    public CommonResult updateOrder(@RequestBody OrderDTO orderDTO) throws SQLException {
//        System.out.println(orderDTO);
        orderService.updateOrder(orderDTO);
        return CommonResultUtils.build(null);
    }

    /**
     * 根据订单id 查询订单
     * @param oid
     * @return
     */
    @GetMapping("/get/{oid}")
    public CommonResult getOrder(@PathVariable("oid") Integer oid) throws JsonProcessingException {
        OrderVO orderVO = orderService.getOrder(oid);
        return CommonResultUtils.build(orderVO);
    }

    @GetMapping("/delete")
    public CommonResult deleteOrder(List<Integer> ids) throws SQLException {
        orderService.deleteOrder(ids);
        return CommonResultUtils.build(null);
    }

    /**
     * 获取用户订单 - 家长- 教师
     * @return
     * @throws Exception
     */
    @PostMapping("/getUserOrder")
    public CommonResult getUserOrder(@RequestBody SearchOrderDTO dto) throws Exception {
        IPage<OrderVO> page1 = orderService.getUserOrder(dto.getUid(), new PageDTO(dto.getPage(), dto.getSize()));
        return CommonResultUtils.build(page1);
    }

    /**
     * 查询所有用户订单，管理端
     * @param searchOrderDTO
     * @return
     */
    @PostMapping("/list")
    public CommonResult getOrderList(@RequestBody SearchOrderDTO searchOrderDTO) throws Exception {
        IPage result = orderService.getOrderList(searchOrderDTO);
        return CommonResultUtils.build(result);
    }

    @GetMapping("/finish/{oid}/{tid}")
    public CommonResult finish(@PathVariable("oid") Integer oid,
                               @PathVariable("tid") Integer tid) throws Exception {
        orderService.finish(oid,tid);
        return CommonResultUtils.build(null);
    }


    /**
     * 取消订单
     * @param oid
     * @param tid
     * @return
     * @throws Exception
     */
    @GetMapping("/cancel/{oid}")
    public CommonResult cancel(@PathVariable("oid") Integer oid
                               ) throws Exception {
        orderService.cancel(oid);
        return CommonResultUtils.build(null);
    }

    @GetMapping("/count")
    public CommonResult getCount() throws Exception {
        return CommonResultUtils.build(orderService.getCount());
    }
}

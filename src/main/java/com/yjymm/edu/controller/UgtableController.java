package com.yjymm.edu.controller;


import com.yjymm.edu.common.CommonResultUtils;
import com.yjymm.edu.model.common.CommonResult;
import com.yjymm.edu.model.entity.Ugtable;
import com.yjymm.edu.model.vo.UgtableVO;
import com.yjymm.edu.service.UgtableService;
import org.apache.http.conn.util.PublicSuffixList;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.sql.SQLException;
import java.util.List;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author yjymm
 * @since 2020-12-25
 */
@RestController
@RequestMapping("/ugtable")
public class UgtableController {

    @Resource
    private UgtableService ugtableService;

    /**
     * 获取用户感兴趣的年级列表
     * @param userId
     * @return
     */
    @GetMapping("/getByUserId/{userId}")
    public CommonResult getByUserId(@PathVariable("userId") Integer userId) {
        List<UgtableVO> ug = ugtableService.getByUserId(userId);
        return CommonResultUtils.build(ug);
    }


    @GetMapping("/get/{ugId}")
    public CommonResult getByUgid(@PathVariable("ugId") Integer ugId) {
        Ugtable ug = ugtableService.getById(ugId);
        return CommonResultUtils.build(ug);
    }


    @GetMapping("/list")
    public CommonResult list() {
        List<UgtableVO> ugtableVOS = ugtableService.getList();
        return CommonResultUtils.build(ugtableVOS);
    }

    @PostMapping("/update/{uid}")
    public CommonResult update(@RequestBody List<Integer> gidList, @PathVariable("uid") Integer uid) throws SQLException {
        ugtableService.update(uid, gidList);
        return null;
    }

}

package com.yjymm.edu.controller;

import com.yjymm.edu.model.dto.TestDTO;
import org.apache.ibatis.annotations.ResultMap;
import org.springframework.web.bind.annotation.*;

import java.math.BigDecimal;

/**
 * @author yjymm
 * @date 2021-04-13 16:33
 */
@RestController
@RequestMapping("/test")
public class TestController {

    @PostMapping("/map")
    public boolean map(@RequestBody TestDTO dto){
        System.out.println(dto);
        return true;
    }

    @GetMapping("/map2")
    public TestDTO map(){
        TestDTO testDTO = new TestDTO();
        testDTO.setMoney(BigDecimal.valueOf(100));
        return testDTO;
    }
}

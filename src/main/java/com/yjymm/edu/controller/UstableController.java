package com.yjymm.edu.controller;


import com.yjymm.edu.common.CommonResultUtils;
import com.yjymm.edu.model.common.CommonResult;
import com.yjymm.edu.model.entity.Ustable;
import com.yjymm.edu.model.vo.UserVO;
import com.yjymm.edu.model.vo.UstableVO;
import com.yjymm.edu.service.UstableService;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.sql.SQLException;
import java.util.List;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author yjymm
 * @since 2020-12-25
 */
@RestController
@RequestMapping("/ustable")
public class UstableController {

    @Resource
    private UstableService ustableService;

    @GetMapping("/get/{id}")
    public CommonResult getByUsid (@PathVariable("id") Integer id){
        UstableVO ustableVO = ustableService.getByUsid(id);
        return CommonResultUtils.build(ustableVO);
    }

    @GetMapping("/getByUserId/{userid}")
    public CommonResult getByUserId (@PathVariable("userid") Integer userid){
        List<UstableVO> ustableVO = ustableService.getByUserId(userid);
        return CommonResultUtils.build(ustableVO);
    }

    @GetMapping("/list")
    public CommonResult list ( ){
        List<UstableVO> ustableVOS = ustableService.getList();
        return CommonResultUtils.build(ustableVOS);

    }


    @PostMapping("/update/{uid}")
    public CommonResult update(@RequestBody List<Integer> sidList, @PathVariable("uid") Integer uid) throws SQLException {
        ustableService.update(uid, sidList);
        return null;
    }


}

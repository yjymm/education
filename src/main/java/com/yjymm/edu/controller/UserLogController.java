package com.yjymm.edu.controller;


import com.yjymm.edu.common.CommonResultUtils;
import com.yjymm.edu.model.common.CommonResult;
import com.yjymm.edu.service.UserLogService;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author yjymm
 * @since 2021-04-02
 */
@RestController
@RequestMapping("/user_log")
public class UserLogController {

    @Resource
    private UserLogService userLogService;

    @GetMapping("/getUncheckCount/{uid}")
    public CommonResult getUncheckCount(@PathVariable("uid") Integer uid) {
        Integer i = userLogService.getUncheckCount(uid);
        return CommonResultUtils.build(i);
    }

    /**
     * 获取用户所有消息
     * @param uid
     * @return
     */
    @GetMapping("/userlist/{uid}")
    public CommonResult userlist(@PathVariable("uid") Integer uid) {
        return CommonResultUtils.build(userLogService.userlist(uid));
    }

    @GetMapping("/read/{lid}")
    public CommonResult read(@PathVariable("lid") Integer lid) {
        userLogService.read(lid);
        return CommonResultUtils.build(null);
    }

    @GetMapping("/readAll/{uid}")
    public CommonResult readAll(@PathVariable("uid") Integer uid) {
        userLogService.readAll(uid);
        return CommonResultUtils.build(null);
    }
}

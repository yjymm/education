package com.yjymm.edu.aop;

import com.yjymm.edu.model.common.CommonResult;
import com.yjymm.edu.model.common.CommonResultEnum;
import lombok.extern.slf4j.Slf4j;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.*;
import org.springframework.stereotype.Component;
import org.springframework.validation.BindingResult;
import org.springframework.validation.ObjectError;
import sun.rmi.runtime.Log;

import java.util.Arrays;
import java.util.List;

/**
 * @author yjymm
 * @date 2020-12-26 0:17
 * <p>
 * 封装controller返回的vo对象，封装成CommonResult对象
 */

//@Aspect
//@Component
//@Slf4j
public class CommonControllerAop {

    @Pointcut("execution( * com.yjymm.edu.controller.*.*(..))")
    public void pointCut() {
    }

    ;

//    @Around("pointCut()")
//    public CommonResult commonAround(ProceedingJoinPoint proceedingJoinPoint) throws Throwable {
//        CommonResult result = null;
//
//        log.info("aop执行了");
//        log.info("controller {}", proceedingJoinPoint.getTarget().toString());
//        log.info("method {}", proceedingJoinPoint.getSignature().toString());
//
//        try {
//            Object[] args = proceedingJoinPoint.getArgs();
//            if (args != null) {
//                log.info("args {}", args);
//            }
//            result = (CommonResult) proceedingJoinPoint.proceed(args);
//            if (result == null) {
//                result = new CommonResult();
//            }
//            result.setInfo(CommonResultEnum.OK);
//        } catch (Throwable throwable) {
//            log.warn(String.valueOf(throwable));
//            throw throwable;
//        }
//        log.info("commonresult: {}", result.toString());
//        return result;
//    }

//    @Before("pointCut()")
//    public CommonResult commonBefore(ProceedingJoinPoint proceedingJoinPoint) throws Throwable {
//        log.info("aop执行了");
//        log.info("controller {}", proceedingJoinPoint.getTarget().toString());
//        log.info("method {}", proceedingJoinPoint.getSignature().toString());
//
//    }

}

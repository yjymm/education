package com.yjymm.edu.mapper;

import com.yjymm.edu.model.entity.AdminLog;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author yjymm
 * @since 2021-02-18
 */
public interface AdminLogMapper extends BaseMapper<AdminLog> {

}

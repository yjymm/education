package com.yjymm.edu.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.yjymm.edu.model.entity.UserLog;
import org.apache.ibatis.annotations.Param;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author yjymm
 * @since 2021-03-30
 */
public interface UserLogMapper extends BaseMapper<UserLog> {

    Integer getUncheckCount(Integer uid);

    void readAll(@Param("uid") Integer uid);
}

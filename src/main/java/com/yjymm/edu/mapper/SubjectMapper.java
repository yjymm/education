package com.yjymm.edu.mapper;

import com.yjymm.edu.model.entity.Subject;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Select;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author yjymm
 * @since 2020-12-25
 */
public interface SubjectMapper extends BaseMapper<Subject> {

    @Select("select count(*) from subject where subject_name = #{subjectName}")
    Integer selectBySubjectName(String subjectName);

    Integer findByName(String subjectName);
}

package com.yjymm.edu.mapper;

import com.yjymm.edu.model.dto.UstableDTO;
import com.yjymm.edu.model.entity.Ustable;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author yjymm
 * @since 2020-12-25
 */
public interface UstableMapper extends BaseMapper<Ustable> {

    Integer deleteUs(UstableDTO ustableDTO);

    Integer insertUs(UstableDTO ustableDTO);

    List<Integer> selectUserInterestingSubjectByUid(@Param("uid") Integer uid);

}

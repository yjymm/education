package com.yjymm.edu.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.yjymm.edu.model.entity.OrderEvaluateImg;

import java.util.List;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author yjymm
 * @since 2021-01-30
 */
public interface OrderEvaluateImgMapper extends BaseMapper<OrderEvaluateImg> {


}

package com.yjymm.edu.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.yjymm.edu.model.entity.MoneyLog;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author yjymm
 * @since 2021-03-30
 */
public interface MoneyLogMapper extends BaseMapper<MoneyLog> {

}

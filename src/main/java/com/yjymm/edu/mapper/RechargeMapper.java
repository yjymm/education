package com.yjymm.edu.mapper;

import com.yjymm.edu.model.entity.Recharge;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author yjymm
 * @since 2020-12-25
 */
public interface RechargeMapper extends BaseMapper<Recharge> {

    Recharge selectByRechargeNum(String rechargeNum);

    Integer getUserRechargeCount(Integer uid);

    Integer getUserSuccessRechargeCount(Integer uid);
}

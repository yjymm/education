package com.yjymm.edu.mapper;

import com.yjymm.edu.model.entity.User;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import java.util.List;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author yjymm
 * @since 2020-12-25
 */
public interface UserMapper extends BaseMapper<User> {

    @Select("select count(*) from user where open_id = #{openId}")
    public Integer isExists(String openId);

    User getUserByOpenId(String openId);

    List<Integer> selectWithRoleId(Integer rid);

    Boolean checkBalance(Integer uid, Double balance);

    Boolean freezeBalance(Integer uid, Double balance);

    User selectByOpenId(String openId);

    String selectAvatarById(@Param("id") Integer id);

    int deleteBatchById(List<Integer> uids);

    // TODO: 2021/3/31 sql语句得检查
    List<Integer> findUserIdsWithTeacherName(@Param("teacherName")String teacherName,@Param("ids") List<Integer> ids);

    List<Integer> selectWithProp(User user);

    User selectAdmin(@Param("username") String username,@Param("password") String password);
}

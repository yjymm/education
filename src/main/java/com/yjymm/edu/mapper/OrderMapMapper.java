package com.yjymm.edu.mapper;

import com.yjymm.edu.model.entity.OrderMap;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

import java.util.List;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author yjymm
 * @since 2020-12-25
 */
public interface OrderMapMapper extends BaseMapper<OrderMap> {

    Integer chooseTeacher(OrderMap orderMap);

    Integer rejectTeacher(OrderMap orderMap);


    List<Integer> selectOidByTid(Integer id);

    List<OrderMap> selectByOid(Integer id);

    Integer getUserOrderCount(Integer uid);
}

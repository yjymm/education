package com.yjymm.edu.mapper;

import com.yjymm.edu.model.entity.Role;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author yjymm
 * @since 2020-12-25
 */
public interface RoleMapper extends BaseMapper<Role> {

}

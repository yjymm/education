package com.yjymm.edu.mapper;

import com.yjymm.edu.model.entity.Grade;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author yjymm
 * @since 2020-12-25
 */
public interface GradeMapper extends BaseMapper<Grade> {

    Integer findByName(String gradeName);
}

package com.yjymm.edu.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.yjymm.edu.model.entity.OrderEvaluate;
import org.apache.ibatis.annotations.Param;
import org.springframework.web.bind.annotation.PathVariable;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author yjymm
 * @since 2021-01-30
 */
public interface OrderEvaluateMapper extends BaseMapper<OrderEvaluate> {

    OrderEvaluate getByOid(@Param("oid") Integer oid);
}

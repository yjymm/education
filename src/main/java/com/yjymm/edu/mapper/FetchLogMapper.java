package com.yjymm.edu.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.yjymm.edu.model.entity.FetchLog;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author yjymm
 * @since 2021-03-21
 */
public interface FetchLogMapper extends BaseMapper<FetchLog> {

}

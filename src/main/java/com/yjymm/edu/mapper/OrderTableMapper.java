package com.yjymm.edu.mapper;

import com.yjymm.edu.model.dto.SearchOrderDTO;
import com.yjymm.edu.model.entity.OrderTable;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;

import java.util.Date;
import java.util.List;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author yjymm
 * @since 2020-12-25
 */
public interface OrderTableMapper extends BaseMapper<OrderTable> {

//    List<OrderTable> selectUnfinishedOrder();

    List<Integer> getAllOid();

//    List<Integer> selectOrderIdWithCreateTime(SearchOrderDTO dto);

    List<Integer> selectOrderIdWithTimeScope(SearchOrderDTO dto);

    List<Integer> selectIdsByDto(SearchOrderDTO dto);

    void cancel(Integer oid);

    Integer getUserOrderCount(Integer uid);
}

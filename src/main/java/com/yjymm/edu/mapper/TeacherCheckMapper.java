package com.yjymm.edu.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.yjymm.edu.model.entity.TeacherCheck;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author yjymm
 * @since 2021-04-20
 */
public interface TeacherCheckMapper extends BaseMapper<TeacherCheck> {

}

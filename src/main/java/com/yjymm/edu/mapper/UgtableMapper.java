package com.yjymm.edu.mapper;

import com.yjymm.edu.model.dto.UgtableDTO;
import com.yjymm.edu.model.entity.Ugtable;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.web.bind.annotation.PathVariable;

import java.util.List;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author yjymm
 * @since 2020-12-25
 */
public interface UgtableMapper extends BaseMapper<Ugtable> {

    Integer deleteUg(UgtableDTO ugtableDTO);

    Integer insertUg(UgtableDTO ugtableDTO);

    List<Integer> selectUserInterestingGradeByUid(@Param("uid") Integer uid);

}

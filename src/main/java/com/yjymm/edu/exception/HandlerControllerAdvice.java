package com.yjymm.edu.exception;

import com.yjymm.edu.common.CommonResultUtils;
import com.yjymm.edu.model.common.CommonResult;
import com.yjymm.edu.model.common.CommonResultEnum;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;

import java.sql.SQLException;

/**
 * @author yjymm
 * @date 2020-12-26 0:19
 *
 * 全局异常处理,根据异常类型，返回对应的信息
 */

@ControllerAdvice
@ResponseBody
public class HandlerControllerAdvice{

    /**
     * 前段参数验证错误产生异常
     * @param e
     * @return
     */
    @ExceptionHandler(value = MethodArgumentNotValidException.class)
    public CommonResult methodError(MethodArgumentNotValidException e){
        CommonResult info = CommonResult.builder()
                .code(CommonResultEnum.PARAMS_ERROR.getCode())
                .message(CommonResultEnum.PARAMS_ERROR.getMessage())
                .result(e.getBindingResult().getAllErrors())
                .build();
        return info;
    }


    /**
     * 返回自定义的消息
     * @param e
     * @return
     */
    @ExceptionHandler(value = MyException.class)
    public CommonResult myExceptionHandler(MyException e){
        CommonResult info = CommonResult.builder()
                .code(CommonResultEnum.MYERROR.getCode())
                .message(e.getMessage())
                .build();
        return info;
    }

    /**
     *
     * @return
     */
    @ExceptionHandler(WxLoginException.class)
    public CommonResult loginError(){
        CommonResult build = CommonResult.builder()
                .code(CommonResultEnum.LOGIN_ERROR.getCode())
                .message(CommonResultEnum.LOGIN_ERROR.getMessage())
                .build();
        return build;
    }

    /**
     * 数据库异常 ( 更新失败，链接失败等)
     * message属性是根据exception的message传入
     * @param e
     * @return
     */
    @ExceptionHandler(SQLException.class)
    public CommonResult sqlException(SQLException e){
        CommonResult build = CommonResult.builder()
                .code(CommonResultEnum.MYSQL_ERROR.getCode())
                .message(e.getMessage())
                .build();
        return build;
    }


    /**
     * 数据库记录已存在，插入失败
     * @param e
     * @return
     */
    @ExceptionHandler(SqlRecordExistsException.class)
    public CommonResult sqlException(SqlRecordExistsException e){
        CommonResult build = CommonResult.builder()
                .code(CommonResultEnum.RECORD_EXISTS.getCode())
                .message(CommonResultEnum.RECORD_EXISTS.getMessage())
                .build();
        return build;
    }

    /**
     * 服务器错误，可捕获最大的Exception
     * @param e
     * @return
     */
    @ExceptionHandler(RuntimeException.class)
    public CommonResult runtimeExceptionHandler(RuntimeException e){
        CommonResult build = CommonResult.builder()
                .code(CommonResultEnum.ERROR.getCode())
                .message(CommonResultEnum.ERROR.getMessage())
                .build();
        return build;
    }

    /**
     * 上传文件异常
     * @param e
     * @return
     */
    @ExceptionHandler(FileUploadException.class)
    public CommonResult fileUploadExceptionHandler(FileUploadException e){
        CommonResult build = CommonResult.builder()
                .code(CommonResultEnum.FILE_UPLOAD_ERROR.getCode())
                .message(e.getMessage())
                .build();
        return build;
    }

    /**
     * 管理员登陆验证失败
     * @param e
     * @return
     */
    @ExceptionHandler(AdminLoginException.class)
    public CommonResult AdminLoginExceptionHandler(AdminLoginException e){
        CommonResult build = CommonResult.builder()
                .code(CommonResultEnum.LOGIN_ERROR.getCode())
                .message(e.getMessage())
                .build();
        return build;
    }
}

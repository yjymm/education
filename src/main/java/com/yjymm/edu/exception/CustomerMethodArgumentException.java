package com.yjymm.edu.exception;

/**
 * @author yjymm
 * @date 2021-01-04 14:50
 */
public class CustomerMethodArgumentException extends CommonBaseException {

    public CustomerMethodArgumentException() {
        super();
    }

    public CustomerMethodArgumentException(String message) {
        super(message);
    }

    public CustomerMethodArgumentException(String message, Throwable cause) {
        super(message, cause);
    }

    public CustomerMethodArgumentException(Throwable cause) {
        super(cause);
    }

    protected CustomerMethodArgumentException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}

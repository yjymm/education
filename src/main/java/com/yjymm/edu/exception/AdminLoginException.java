package com.yjymm.edu.exception;

/**
 * @author yjymm
 * @date 2021-01-13 23:51
 */
public class AdminLoginException extends CommonBaseException {

    public AdminLoginException() {
        super();
    }

    public AdminLoginException(String message) {
        super(message);
    }

    public AdminLoginException(String message, Throwable cause) {
        super(message, cause);
    }

    public AdminLoginException(Throwable cause) {
        super(cause);
    }

    protected AdminLoginException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}

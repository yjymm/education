package com.yjymm.edu.exception;

/**
 * @author yjymm
 * @date 2020-12-26 15:03
 */
public class MyException extends CommonBaseException{

    public MyException() {

    }

    public MyException(String message) {
        super(message);
    }

    public MyException(String message, Throwable cause) {
        super(message, cause);
    }

    public MyException(Throwable cause) {
        super(cause);
    }

    protected MyException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}

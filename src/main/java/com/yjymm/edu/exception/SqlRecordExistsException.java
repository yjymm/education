package com.yjymm.edu.exception;

/**
 * @author yjymm
 * @date 2021-01-03 23:35
 * 数据库中已经存在该条记录
 */
public class SqlRecordExistsException extends CommonBaseException{

    public SqlRecordExistsException() {
        super();
    }

    public SqlRecordExistsException(String message) {
        super(message);
    }

    public SqlRecordExistsException(String message, Throwable cause) {
        super(message, cause);
    }

    public SqlRecordExistsException(Throwable cause) {
        super(cause);
    }

    protected SqlRecordExistsException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}

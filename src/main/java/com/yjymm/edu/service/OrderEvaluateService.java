package com.yjymm.edu.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.yjymm.edu.model.common.CommonResult;
import com.yjymm.edu.model.dto.OrderEvaluateDTO;
import com.yjymm.edu.model.entity.OrderEvaluate;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author yjymm
 * @since 2021-01-30
 */
public interface OrderEvaluateService extends IService<OrderEvaluate> {

    void insert(OrderEvaluateDTO dto) throws Exception;

    CommonResult uploadImg(MultipartFile multipartFile) throws Exception;
}

package com.yjymm.edu.service;

import com.baomidou.mybatisplus.core.metadata.IPage;

/**
 * @author yjymm
 * @date 2021-03-31 0:01
 */
public interface RecommendService {
    IPage recommendedTeacher(Integer uid);
}

package com.yjymm.edu.service;

import com.yjymm.edu.model.dto.OrderMapDTO;
import com.yjymm.edu.model.entity.OrderMap;
import com.baomidou.mybatisplus.extension.service.IService;
import com.yjymm.edu.model.vo.OrderMapVO;

import java.sql.SQLException;
import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author yjymm
 * @since 2020-12-25
 */
public interface OrderMapService extends IService<OrderMap> {

    void insert(OrderMapDTO orderMapDTO) throws SQLException, Exception;

    void updateOrderMap(OrderMapDTO orderMapDTO) throws SQLException;

    void chooseTeacher(OrderMapDTO orderMapDTO) throws Exception;

    List<OrderMapVO> listByOid(OrderMapDTO orderMapDTO);

    void robOrder(Integer oid, Integer tid) throws SQLException;
}

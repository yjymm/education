package com.yjymm.edu.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.IService;
import com.yjymm.edu.model.dto.FetchMoneyDTO;
import com.yjymm.edu.model.dto.FetchResultDTO;
import com.yjymm.edu.model.dto.FetchSearchDTO;
import com.yjymm.edu.model.entity.FetchLog;

import java.sql.SQLException;
import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author yjymm
 * @since 2021-03-21
 */
public interface FetchLogService extends IService<FetchLog> {

    void insertFetch(FetchMoneyDTO fetchMoneyDTO) throws SQLException;

    void cancel(Integer fid, Integer id);

    void checkFecthLog(FetchResultDTO fetchLog);

    List<FetchLog> getList(Integer uid);

    IPage searchList(FetchSearchDTO dto);

    Integer getCount();
}

package com.yjymm.edu.service;

import com.yjymm.edu.model.entity.Ustable;
import com.baomidou.mybatisplus.extension.service.IService;
import com.yjymm.edu.model.vo.UstableVO;

import java.sql.SQLException;
import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author yjymm
 * @since 2020-12-25
 */
public interface UstableService extends IService<Ustable> {

    UstableVO getByUsid(Integer id);

    List<UstableVO> getList();

    List<UstableVO> getByUserId(Integer userid);

    void update(Integer uid, List<Integer> sidList) throws SQLException;

    List<Integer> getUserFarvoriteSubjectIds(Integer uid);
}

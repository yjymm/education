package com.yjymm.edu.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.yjymm.edu.model.entity.TeacherCheck;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author yjymm
 * @since 2021-04-20
 */
public interface TeacherCheckService extends IService<TeacherCheck> {

}

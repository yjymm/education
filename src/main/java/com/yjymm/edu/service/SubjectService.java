package com.yjymm.edu.service;

import com.yjymm.edu.model.dto.SubjectDTO;
import com.yjymm.edu.model.entity.Subject;
import com.baomidou.mybatisplus.extension.service.IService;
import com.yjymm.edu.model.vo.SubjectVO;

import java.sql.SQLException;
import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author yjymm
 * @since 2020-12-25
 */
public interface SubjectService extends IService<Subject> {

    SubjectVO insert(SubjectDTO subjectDTO) throws SQLException;


    List<SubjectVO> listUserFavoriteSubjectByOpenId(String openId);

    void insertSubject(Subject s) throws Exception;
}

package com.yjymm.edu.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.yjymm.edu.model.entity.UserLog;

import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author yjymm
 * @since 2021-04-02
 */
public interface UserLogService extends IService<UserLog> {

    Integer getUncheckCount(Integer uid);

    List<UserLog> userlist(Integer uid);

    void read(Integer lid);

    void readAll(Integer uid);
}

package com.yjymm.edu.service;

import com.yjymm.edu.model.entity.Grade;
import com.baomidou.mybatisplus.extension.service.IService;
import com.yjymm.edu.model.vo.GradeVO;
import org.springframework.transaction.annotation.Transactional;

import java.sql.SQLException;
import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author yjymm
 * @since 2020-12-25
 */
public interface GradeService extends IService<Grade> {

    Boolean insert(Grade grade);

    @Transactional(rollbackFor = Exception.class)
    Boolean delete(Integer gid) throws SQLException;

    int deleteBatchById(List<Integer> gids);

    List<GradeVO> listUserFavoriteByOpenId(String openId);

    void insertGrade(Grade grade) throws Exception;

    void updateGrade(Grade grade);
}

package com.yjymm.edu.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.yjymm.edu.model.dto.AdminLoginDTO;
import com.yjymm.edu.model.dto.AdminSearchDTO;
import com.yjymm.edu.model.dto.AdminUpdateDTO;
import com.yjymm.edu.model.vo.AdminLoginVo;
import org.springframework.transaction.annotation.Transactional;

/**
 * @author yjymm
 * @date 2021-04-21 9:36
 */
public interface AdminService {
    AdminLoginVo adminLogin(AdminLoginDTO dto) throws Exception;

    @Transactional
    void insertAdmin(AdminLoginDTO dto);

    IPage list(AdminSearchDTO dto);

    void updatePass(AdminUpdateDTO dto);

    void statusOpt(Integer adminId, Integer code);
}

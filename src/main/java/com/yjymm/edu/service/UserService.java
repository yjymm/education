package com.yjymm.edu.service;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.yjymm.edu.model.dto.*;
import com.yjymm.edu.model.entity.User;
import com.baomidou.mybatisplus.extension.service.IService;
import com.yjymm.edu.model.vo.AdminLoginVo;
import com.yjymm.edu.model.vo.OrderVO;
import com.yjymm.edu.model.vo.UserVO;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.sql.SQLException;
import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author yjymm
 * @since 2020-12-25
 */
public interface UserService extends IService<User> {

    UserVO getOpenId(String code) throws Exception;

    UserVO insert(UserDTO userDTO) throws SQLException;

    UserVO getByOpenId(String openId) throws Exception;

    UserVO updateUser(UserDTO userDTO) throws SQLException;

    void uploadAvatar(Integer id, MultipartFile file) throws IOException, SQLException;

    UserVO getDetailById(Integer id) throws SQLException, Exception;

    IPage<UserVO> listPageWithWrapper(Long size, Long page, QueryWrapper<User> wrapper);

    void delete(List<Integer> uids);

    void updateUsAndUg(UpdateUsUgDTO dto) throws SQLException;

    UserVO getTeacherDetaiById(Integer tid) throws Exception;

    Integer getCount();

    void changeRole(ChangeRoleDTO dto);
}

package com.yjymm.edu.service;

import com.yjymm.edu.model.entity.Role;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author yjymm
 * @since 2020-12-25
 */
public interface RoleService extends IService<Role> {

}

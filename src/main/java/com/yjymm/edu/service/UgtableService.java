package com.yjymm.edu.service;

import com.yjymm.edu.model.entity.Ugtable;
import com.baomidou.mybatisplus.extension.service.IService;
import com.yjymm.edu.model.vo.UgtableVO;
import org.springframework.transaction.annotation.Transactional;

import java.sql.SQLException;
import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author yjymm
 * @since 2020-12-25
 */
public interface UgtableService extends IService<Ugtable> {


    List<UgtableVO> getList();

    void update(Integer uid, List<Integer> gidList) throws SQLException;

    List<UgtableVO> getByUserId(Integer userId);

    List<Integer> getUserFarvoriteGradeIds(Integer uid);
}

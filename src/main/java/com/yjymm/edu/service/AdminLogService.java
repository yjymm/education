package com.yjymm.edu.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.yjymm.edu.model.dto.AdminLogDTO;
import com.yjymm.edu.model.entity.AdminLog;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author yjymm
 * @since 2021-02-18
 */
public interface AdminLogService extends IService<AdminLog> {

    IPage getList(AdminLogDTO dto);

    void read(Integer lid);
}

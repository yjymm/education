package com.yjymm.edu.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.yjymm.edu.model.entity.OrderEvaluateImg;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author yjymm
 * @since 2021-01-30
 */
public interface OrderEvaluateImgService extends IService<OrderEvaluateImg> {

}

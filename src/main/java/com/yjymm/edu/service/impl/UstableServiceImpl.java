package com.yjymm.edu.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.yjymm.edu.common.MyBeanUtils;
import com.yjymm.edu.common.RedisUtils;
import com.yjymm.edu.constant.UgtableConstant;
import com.yjymm.edu.constant.UstableConstant;
import com.yjymm.edu.model.dto.UstableDTO;
import com.yjymm.edu.model.entity.Ustable;
import com.yjymm.edu.mapper.UstableMapper;
import com.yjymm.edu.model.vo.UstableVO;
import com.yjymm.edu.service.UstableService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.BeanUtils;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;

import javax.annotation.Resource;
import java.sql.SQLException;
import java.util.List;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author yjymm
 * @since 2020-12-25
 */
@Service
public class UstableServiceImpl extends ServiceImpl<UstableMapper, Ustable> implements UstableService {

    @Resource
    private UstableMapper ustableMapper;

    @Resource
    private RedisUtils redisUtils;

    @Override
    public UstableVO getByUsid(Integer id) {
        Ustable ustable = baseMapper.selectById(id);
        UstableVO ustableVO = new UstableVO();
        if (ustable != null) {
            BeanUtils.copyProperties(ustable,ustableVO);
            return ustableVO;
        }
        return null;
    }

    @Override
    public List<UstableVO> getList() {
        List<Ustable> ustables = baseMapper.selectList(null);
        if (CollectionUtils.isEmpty(ustables)) {
            return null;
        }
        List<UstableVO> ugtableVOS = MyBeanUtils.converToList(ustables, UstableVO.class);
        return ugtableVOS;
    }

    @Override
    public List<UstableVO> getByUserId(Integer userid) {
        QueryWrapper<Ustable> wrapper = new QueryWrapper<>();
        wrapper.eq("uid", userid);
        List<Ustable> ustables = baseMapper.selectList(wrapper);
        if (ustables == null) {
            return null;
        }
        List<UstableVO> ustableVOS = MyBeanUtils.converToList(ustables, UstableVO.class);
        return ustableVOS;

    }

    /**
     * 更新用户感兴趣的科目
     * @param uid
     * @param sidList
     * @throws SQLException
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public void update(Integer uid, List<Integer> sidList) throws SQLException {
        UstableDTO ustable = new UstableDTO();
        ustable.setUid(uid);
        ustableMapper.deleteUs(ustable);

        // 判断sidlist是否未空
        if (CollectionUtils.isEmpty(sidList)) {
            // 删除缓存
            redisUtils.delete(UstableConstant.US_USER_INTERESTED_KEY + uid);
            return;
        }

        // 重新插入
        UstableDTO ustableDTO = new UstableDTO();
        ustableDTO.setUid(uid);
        ustableDTO.setSids(sidList);
        Integer integer = ustableMapper.insertUs(ustableDTO);

        if (integer != sidList.size()) {
            throw new SQLException("插入失败");
        }
        // 删除缓存
        redisUtils.delete(UstableConstant.US_USER_INTERESTED_KEY + uid);
    }

    /**
     * 获取用户感兴趣的subject List
     * @param uid
     * @return
     */
    @Override
    public List<Integer> getUserFarvoriteSubjectIds(Integer uid) {
        // 先查询redis，有则返回
        List<Integer> result = redisUtils.selectList(UstableConstant.US_USER_INTERESTED_KEY + uid, Integer.class);
        if (!CollectionUtils.isEmpty(result)) return result;

        // redis中没有，查询数据库，更新redis
        List<Integer> result2 = baseMapper.selectUserInterestingSubjectByUid(uid);
        if (!CollectionUtils.isEmpty(result2)) {
            // 更新redis
            redisUtils.insertList(UstableConstant.US_USER_INTERESTED_KEY + uid, result2);
            return result2;
        }
        // 数据库中都没有，返回空
        return null;
    }
}

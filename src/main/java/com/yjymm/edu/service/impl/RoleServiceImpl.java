package com.yjymm.edu.service.impl;

import com.yjymm.edu.model.entity.Role;
import com.yjymm.edu.mapper.RoleMapper;
import com.yjymm.edu.service.RoleService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author yjymm
 * @since 2020-12-25
 */
@Service
public class RoleServiceImpl extends ServiceImpl<RoleMapper, Role> implements RoleService {

}

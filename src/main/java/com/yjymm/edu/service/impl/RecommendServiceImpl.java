package com.yjymm.edu.service.impl;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.yjymm.edu.mapper.UgtableMapper;
import com.yjymm.edu.mapper.UserMapper;
import com.yjymm.edu.mapper.UstableMapper;
import com.yjymm.edu.service.RecommendService;
import com.yjymm.edu.service.UgtableService;
import com.yjymm.edu.service.UstableService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * @author yjymm
 * @date 2021-03-31 0:01
 */
@Service
public class RecommendServiceImpl implements RecommendService {

    @Resource
    private UgtableService ugtableService;

    private UgtableMapper ugtableMapper;
    private UserMapper userMapper;
    private UstableMapper ustableMapper;

    @Resource
    private UstableService ustableService;

    @Override
    public IPage recommendedTeacher(Integer uid) {
        // 查询用户感兴趣的年级和科目
        List<Integer> gradeIds = ugtableMapper.selectUserInterestingGradeByUid(uid);
        List<Integer> subjectIds = ustableMapper.selectUserInterestingSubjectByUid(uid);


        
        return null;
    }
}

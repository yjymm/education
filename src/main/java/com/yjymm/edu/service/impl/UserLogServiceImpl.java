package com.yjymm.edu.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.yjymm.edu.mapper.UserLogMapper;
import com.yjymm.edu.model.entity.UserLog;
import com.yjymm.edu.service.UserLogService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author yjymm
 * @since 2021-04-02
 */
@Service
public class UserLogServiceImpl extends ServiceImpl<UserLogMapper, UserLog> implements UserLogService {

    @Override
    public Integer getUncheckCount(Integer uid) {
        return baseMapper.getUncheckCount(uid);
    }

    @Override
    public List<UserLog> userlist(Integer uid) {
        Map<String, Object> map = new HashMap<>();
        map.put("uid", uid);
        List<UserLog> userLogs = baseMapper.selectByMap(map);
        userLogs.sort((x, y) -> {
            return y.getCreateTime().compareTo(x.getCreateTime());
        });
        return userLogs;
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void read(Integer lid) {
        UserLog build = UserLog.builder()
                .id(lid)
                .state(1)
                .build();
        baseMapper.updateById(build);
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void readAll(Integer uid) {
        baseMapper.readAll(uid);
    }
}

package com.yjymm.edu.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.yjymm.edu.common.MyBeanUtils;
import com.yjymm.edu.common.RedisUtils;
import com.yjymm.edu.constant.UgtableConstant;
import com.yjymm.edu.constant.UstableConstant;
import com.yjymm.edu.model.dto.UgtableDTO;
import com.yjymm.edu.model.entity.Ugtable;
import com.yjymm.edu.mapper.UgtableMapper;
import com.yjymm.edu.model.vo.UgtableVO;
import com.yjymm.edu.service.UgtableService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.interceptor.TransactionAspectSupport;
import org.springframework.util.CollectionUtils;

import javax.annotation.Resource;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * @author yjymm
 * @since 2020-12-25
 */
@Service
public class UgtableServiceImpl extends ServiceImpl<UgtableMapper, Ugtable> implements UgtableService {

    @Resource
    private RedisUtils redisUtils;

    /**
     * 获取用户兴趣的年级对象
     * @param userId
     * @return
     */
    @Override
    public List<UgtableVO> getByUserId(Integer userId) {
        QueryWrapper<Ugtable> query = new QueryWrapper<>();
        query.eq("uid", userId);
        List<Ugtable> ugtables = baseMapper.selectList(query);
        if (ugtables.isEmpty()) {
            return null;
        }
        List<UgtableVO> ugtableVOS = MyBeanUtils.converToList(ugtables, UgtableVO.class);
        return ugtableVOS;
    }

    /**
     * 返回所有用户-年级映射关系
     * @return
     */
    @Override
    public List<UgtableVO> getList() {
        List<Ugtable> ugtables = baseMapper.selectList(null);
        if (CollectionUtils.isEmpty(ugtables)) {
            return null;
        }
        List<UgtableVO> ugtableVOS = MyBeanUtils.converToList(ugtables, UgtableVO.class);
        return ugtableVOS;
    }

    /**
     * 更新用户感兴趣的年级
     * @param uid
     * @param gidList
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public void update(Integer uid, List<Integer> gidList) throws SQLException {
        // 删除原来的数据库的用户感兴趣的年级的记录
        UgtableDTO ugtable = new UgtableDTO();
        ugtable.setUid(uid);
        Integer deleteCount = baseMapper.deleteUg(ugtable);

        // 判断sidlist是否未空
        if (CollectionUtils.isEmpty(gidList)) {
            // 删除缓存
            redisUtils.delete(UgtableConstant.UG_USER_INTERESTED_KEY + uid);
            return;
        }

        // 去重
        gidList = gidList.stream().distinct().collect(Collectors.toList());

        // 插入新的记录
        UgtableDTO dto = new UgtableDTO();
        dto.setGids(gidList);
        dto.setUid(uid);
        Integer insert = baseMapper.insertUg(dto);

        // 删除redis缓存
        redisUtils.delete(UgtableConstant.UG_USER_INTERESTED_KEY + uid);

        if (insert != gidList.size()) {
            throw new SQLException("更新失败");
        }
    }

    /**
     * 查询用户感兴趣的年级id列表
     * @param uid
     * @return
     */
    @Override
    public List<Integer> getUserFarvoriteGradeIds(Integer uid) {
        // 先查询redis，有则返回
        List<Integer> result = redisUtils.selectList(UgtableConstant.UG_USER_INTERESTED_KEY + uid, Integer.class);
        if (!CollectionUtils.isEmpty(result)) return result;

        // redis中没有，查询数据库，更新redis
        List<Integer> result2 = baseMapper.selectUserInterestingGradeByUid(uid);
        if (!CollectionUtils.isEmpty(result2)) {
            // 更新redis
            redisUtils.insertList(UgtableConstant.UG_USER_INTERESTED_KEY + uid, result2);
            return result2;
        }
        // 数据库中都没有，返回空
        return null;
    }
}

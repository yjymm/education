package com.yjymm.edu.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.yjymm.edu.common.MyBeanUtils;
import com.yjymm.edu.config.MybatisConfig;
import com.yjymm.edu.exception.MyException;
import com.yjymm.edu.mapper.OrderTableMapper;
import com.yjymm.edu.mapper.UserMapper;
import com.yjymm.edu.model.dto.OrderMapDTO;
import com.yjymm.edu.model.entity.OrderMap;
import com.yjymm.edu.mapper.OrderMapMapper;
import com.yjymm.edu.model.entity.OrderTable;
import com.yjymm.edu.model.entity.User;
import com.yjymm.edu.model.vo.OrderMapVO;
import com.yjymm.edu.service.OrderMapService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.apache.log4j.spi.ThrowableInformation;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.sql.SQLException;
import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 *
 * @author yjymm
 * @since 2020-12-25
 */
@Service
public class OrderMapServiceImpl extends ServiceImpl<OrderMapMapper, OrderMap> implements OrderMapService {

    @Resource
    private UserMapper userMapper;

    @Resource
    private OrderMapMapper orderMapMapper;

    @Resource
    private OrderTableMapper orderTableMapper;

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void insert(OrderMapDTO orderMapDTO) throws Exception {
        // 查看该教师是否已经接过单
        Map<String, Object> map = new HashMap<>();
        map.put("oid", orderMapDTO.getOid());
        map.put("tid", orderMapDTO.getTid());
        List<OrderMap> orderMapList = baseMapper.selectByMap(map);
        if (!CollectionUtils.isEmpty(orderMapList)) {
            throw new MyException("您已接单，无需重复接单");
        }
        OrderMap orderMap = new OrderMap();
        orderMap.setCreateTime(LocalDateTime.now());
        orderMap.setState(0);
        BeanUtils.copyProperties(orderMapDTO, orderMap);
        boolean save = save(orderMap);
        if (!save) {
            throw new SQLException("操作失败");
        }
    }

    @Override
    public void updateOrderMap(OrderMapDTO orderMapDTO) throws SQLException {
        OrderMap orderMap = new OrderMap();
        BeanUtils.copyProperties(orderMapDTO, orderMap);
        boolean b = updateById(orderMap);
        if (!b) {
            throw new SQLException("操作失败");
        }
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void chooseTeacher(OrderMapDTO orderMapDTO) throws Exception {
        OrderMap orderMap = new OrderMap();
        BeanUtils.copyProperties(orderMapDTO, orderMap);
        orderMapMapper.rejectTeacher(orderMap);
        orderMapMapper.chooseTeacher(orderMap);
        // 修改订单状态
        OrderTable orderTable = new OrderTable();
        orderTable.setId(orderMap.getOid());
        orderTable.setFinished(2);
        orderTableMapper.updateById(orderTable);

        // 修改余额，教师加上相关冻结余额
        OrderTable orderTable1 = orderTableMapper.selectById(orderMap.getOid());
        BigDecimal money = orderTable1.getMoney();
        User user = userMapper.selectById(orderMap.getTid());
        user.setFreezeBalance(user.getFreezeBalance().add(money));
        userMapper.updateById(user);
    }

    @Override
    public List<OrderMapVO> listByOid(OrderMapDTO orderMapDTO) {
//        OrderMap orderMap = MyBeanUtils.copyProperties2Object(orderMapDTO, OrderMap.class);
        QueryWrapper<OrderMap> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("oid", orderMapDTO.getOid());
        List<OrderMap> orderMaps = baseMapper.selectList(queryWrapper);
        List<OrderMapVO> orderMapVOS = MyBeanUtils.converToList(orderMaps, OrderMapVO.class);
        return orderMapVOS;
    }

    @Override
    public void robOrder(Integer oid, Integer tid) throws SQLException {
        OrderTable orderTable = orderTableMapper.selectById(oid);
        if (orderTable.getFinished() == 0) {
            OrderMap orderMap = new OrderMap();
            orderMap.setOid(oid);
            orderMap.setTid(tid);
            int insert = baseMapper.insert(orderMap);
            if (insert != 1) {
                throw new SQLException("投递失败，请稍后重试");
            }
        } else {
            throw new SQLException("投递失败，该订单正在进行中,不能继续投递");
        }
    }
}

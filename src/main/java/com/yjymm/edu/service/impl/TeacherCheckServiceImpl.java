package com.yjymm.edu.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.yjymm.edu.mapper.TeacherCheckMapper;
import com.yjymm.edu.model.entity.TeacherCheck;
import com.yjymm.edu.service.TeacherCheckService;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author yjymm
 * @since 2021-04-20
 */
@Service
public class TeacherCheckServiceImpl extends ServiceImpl<TeacherCheckMapper, TeacherCheck> implements TeacherCheckService {

}

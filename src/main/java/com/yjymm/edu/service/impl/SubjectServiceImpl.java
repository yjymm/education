package com.yjymm.edu.service.impl;

import com.yjymm.edu.common.MyBeanUtils;
import com.yjymm.edu.exception.MyException;
import com.yjymm.edu.exception.SqlRecordExistsException;
import com.yjymm.edu.mapper.UserMapper;
import com.yjymm.edu.mapper.UstableMapper;
import com.yjymm.edu.model.dto.SubjectDTO;
import com.yjymm.edu.model.entity.Subject;
import com.yjymm.edu.mapper.SubjectMapper;
import com.yjymm.edu.model.entity.User;
import com.yjymm.edu.model.vo.SubjectVO;
import com.yjymm.edu.service.SubjectService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.yjymm.edu.service.UstableService;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.sql.SQLException;
import java.util.List;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author yjymm
 * @since 2020-12-25
 */
@Service
public class SubjectServiceImpl extends ServiceImpl<SubjectMapper, Subject> implements SubjectService {

    @Resource
    private UserMapper userMapper;

    @Resource
    private UstableMapper ustableMapper;

    @Resource
    private UstableService ustableService;

    @Override
    public SubjectVO insert(SubjectDTO subjectDTO) throws SQLException {
        Subject subject = MyBeanUtils.copyProperties2Object(subjectDTO, Subject.class);
        Integer hasSubject = baseMapper.selectBySubjectName(subject.getSubjectName());
        if (hasSubject != null){
            // 已经存在该记录，不再插入
            throw new SqlRecordExistsException("该条数据已存在");
        }
        int insert = baseMapper.insert(subject);
        if (insert != 1){
            // 插入失败
            throw new SQLException("插入失败");
        }
        return null;
    }

    /**
     * 查询所有科目，用户感兴趣的科目就ischecked = true
     * @param openId
     * @return
     */
    @Override
    public List<SubjectVO> listUserFavoriteSubjectByOpenId(String openId) {
        User user = userMapper.selectByOpenId(openId);
        List<Subject> subjects = baseMapper.selectList(null);

        List<Integer> list = ustableService.getUserFarvoriteSubjectIds(user.getId());


        List<SubjectVO> subjectVOS = MyBeanUtils.converToList(subjects, SubjectVO.class);

        if(list == null || list.size() == 0) {
            return subjectVOS;
        }
        for (SubjectVO subjectVO : subjectVOS) {
            if (list.contains(subjectVO.getId())) {
                subjectVO.setIsChecked(true);
            }
        }

        return subjectVOS;
    }

    @Transactional(rollbackFor = Exception.class)
    @Override
    public void insertSubject(Subject s) throws Exception{
        Integer i = baseMapper.findByName(s.getSubjectName());
        if (i >= 1) {
            throw new SqlRecordExistsException("记录以存在，添加失败");
        }
        s.setDeleted(0);
        int insert = baseMapper.insert(s);
        if (insert != 1) {
            throw new MyException("插入记录失败");
        }
    }
}

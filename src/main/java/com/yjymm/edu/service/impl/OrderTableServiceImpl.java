package com.yjymm.edu.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.yjymm.edu.common.*;
import com.yjymm.edu.common.constant.AdminLogConstant;
import com.yjymm.edu.common.constant.UserLogConstant;
import com.yjymm.edu.constant.OrderConstant;
import com.yjymm.edu.exception.MyException;
import com.yjymm.edu.mapper.*;
import com.yjymm.edu.model.common.OrderTimeEnum;
import com.yjymm.edu.model.common.RoleEnum;
import com.yjymm.edu.model.dto.OrderDTO;
import com.yjymm.edu.model.dto.PageDTO;
import com.yjymm.edu.model.dto.SearchOrderDTO;
import com.yjymm.edu.model.entity.*;
import com.yjymm.edu.model.vo.OrderEvaluateVO;
import com.yjymm.edu.model.vo.OrderMapVO;
import com.yjymm.edu.model.vo.OrderVO;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.yjymm.edu.model.vo.UserVO;
import com.yjymm.edu.service.OrderTableService;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.sql.SQLException;
import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.util.*;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.stream.Collectors;

/**
 * @author yjymm
 * @since 2020-12-25
 */
@Service
public class OrderTableServiceImpl extends ServiceImpl<OrderTableMapper, OrderTable> implements OrderTableService {

    @Resource
    private UserLogMapper userLogMapper;

    @Resource
    private MoneyLogMapper moneyLogMapper;

    @Resource
    private AdminLogMapper adminLogMapper;

    @Resource
    private RedisUtils redisUtils;

    @Resource
    private UserMapper userMapper;

    @Resource
    private GradeMapper gradeMapper;

    @Resource
    private SubjectMapper subjectMapper;

    @Resource
    private OrderMapMapper orderMapMapper;

    @Resource
    private OrderTableMapper orderTableMapper;

    @Resource
    private OrderEvaluateMapper orderEvaluateMapper;

    @Resource
    private OSSUtils ossUtils;

    /**
     * 家长发布一个订单
     *
     * @param orderDTO
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public void dispatchOrder(OrderDTO orderDTO) throws SQLException {

        OrderTable order = MyBeanUtils.copyProperties2Object(orderDTO, OrderTable.class);
        User user = userMapper.selectById(orderDTO.getPid());

        // 判断账户余额是否足够
//        Boolean aBoolean = user.getBalance() > orderDTO.getMoney() ? true : false;
        Boolean aBoolean = user.getBalance().compareTo(orderDTO.getMoney()) > 0 ? true : false;
        if (!aBoolean) {
            throw new MyException("余额不足，操作失败，请充值");
        }

        // 冻结相关金额
//        user.setBalance(user.getBalance() - orderDTO.getMoney());
//        user.setFreezeBalance(user.getFreezeBalance() + orderDTO.getMoney());
        user.setBalance(user.getBalance().subtract(orderDTO.getMoney()));
        user.setFreezeBalance(user.getFreezeBalance().add(orderDTO.getMoney()));
        int update = userMapper.updateById(user);
        if (update != 1) {
            throw new MyException("冻结账户订单金额失败，请稍后重试");
        }

        List<Integer> gids = orderDTO.getGids();
        StringBuilder gidstr = new StringBuilder();
        for (int i = 0; i < gids.size(); i++) {
            gidstr.append(gids.get(i) + ";");
        }
        List<Integer> sids = orderDTO.getSids();
        StringBuilder sidstr = new StringBuilder();
        for (int i = 0; i < sids.size(); i++) {
            sidstr.append(sids.get(i) + ";");
        }
        order.setGids(gidstr.toString());
        order.setSids(sidstr.toString());
        order.setUsername(user.getUsername());
        order.setRealName(user.getRealName());
        order.setOrderNum(StringUtils.getRandomString("order-"));
        order.setCreateTime(LocalDateTime.now());

        int insert = baseMapper.insert(order);
        if (insert != 1) {
            throw new SQLException("发布订单失败，请稍后重试");
        }

        // 通知管理员
        AdminLog adminLog = new AdminLog();
        adminLog.setCreateTime(LocalDateTime.now());
        adminLog.setState(0);
        adminLog.setLogTime(LocalDateTime.now());
        String description = String.format(AdminLogConstant.USER_DISPATCH_ORDER_NOTICE,
                user.getUsername(), user.getId(), DateTimeUtils.formatStr(LocalDateTime.now()),
                order.getId());
        adminLog.setDescription(description);
        adminLogMapper.insert(adminLog);

        // 记录用户账户变化 money_log,user_log,
        MoneyLog moneyLog = MoneyLog.builder()
                .freezeBalance(user.getFreezeBalance())
                .balance(user.getBalance())
                .money(orderDTO.getMoney())
                .state(0)
                .createTime(LocalDateTime.now())
                .uid(user.getId())
                .description("发布订单冻结金额")
                .build();
        moneyLogMapper.insert(moneyLog);

        String format = String.format(UserLogConstant.USER_DISPATCH_ORDER, DateTimeUtils.formatStr(LocalDateTime.now()), order.getOrderNum());
        UserLog userLog = UserLog.builder()
                .createTime(LocalDateTime.now())
                .state(0)
                .description(format)
                .uid(user.getId())
                .build();
        userLogMapper.insert(userLog);

        // 设置此订单相关的年级，科目缓存
        redisUtils.insertList(OrderConstant.ORDER_WITH_GRADE_KEY + order.getId(), orderDTO.getGids());
        redisUtils.insertList(OrderConstant.ORDER_WITH_SUBJECT_KEY + order.getId(), orderDTO.getSids());
    }

    /**
     * 根究订单id获取订单和订单评价
     *
     * @param id
     * @return
     * @throws JsonProcessingException
     */
    @Override
    public OrderVO getOrder(Integer id) throws JsonProcessingException {
        OrderTable o = baseMapper.selectById(id);
        if (o == null) {
            return null;
        }
        // 获取订单评价和评价图片
        OrderEvaluate orderEvaluate = orderEvaluateMapper.getByOid(id);
        OrderEvaluateVO orderEvaluateVO = null;
        if (orderEvaluate != null) {
            orderEvaluateVO = MyBeanUtils.copyProperties2Object(orderEvaluate, OrderEvaluateVO.class);
            if (!StringUtils.isBlank(orderEvaluate.getImgs())) {
                String[] imgs = orderEvaluate.getImgs().split(";");
                List<String> list = ossUtils.addUrlPrefixArray(imgs);
                orderEvaluateVO.setImgsUrl(list);
            }
        }

        OrderVO orderVO = new OrderVO();
        BeanUtils.copyProperties(o, orderVO);
        orderVO.setOrderEvaluateVO(orderEvaluateVO);

        List<Grade> grades = gradeMapper.selectList(null);
        Map<Integer, String> gidTitlemap = new HashMap<>(grades.size());
        for (Grade grade : grades) {
            gidTitlemap.put(grade.getId(), grade.getGradeName());
        }
        List<Subject> subjects = subjectMapper.selectList(null);
        Map<Integer, String> sidTitlemap = new HashMap<>(subjects.size());
        for (Subject subject : subjects) {
            sidTitlemap.put(subject.getId(), subject.getSubjectName());
        }

        String gids = orderVO.getGids();
        String sids = orderVO.getSids();
        if (!StringUtils.isBlank(gids)) {
            String gidsTitle = "";
            String[] split = gids.split(";");
            for (String s : split) {
                Integer gid = Integer.valueOf(s);
                gidsTitle += gidTitlemap.get(gid) + "、";
            }
            orderVO.setGradeTitle(gidsTitle);
        }

        if (!StringUtils.isBlank(sids)) {
            String sidsTitle = "";
            String[] split = sids.split(";");
            for (String s : split) {
                Integer sid = Integer.valueOf(s);
                sidsTitle += sidTitlemap.get(sid) + "、";
            }
            orderVO.setSubjectTitle(sidsTitle);
        }
        orderVO.setWorkLocation(orderVO.getCountry() +
                orderVO.getProvince() +
                orderVO.getCity() +
                orderVO.getLocation() +
                orderVO.getDetailLocation());

        //获取接单用户列表
        List<OrderMap> orderMapList = orderMapMapper.selectByOid(id);
        List<OrderMapVO> orderMapVOS = MyBeanUtils.converToList(orderMapList, OrderMapVO.class);
        if (!CollectionUtils.isEmpty(orderMapVOS)) {
            for (OrderMapVO orderMapVO : orderMapVOS) {
                User user = userMapper.selectById(orderMapVO.getTid());
                orderMapVO.setTeacherName(user.getUsername());
                orderMapVO.setTeacherPhone(user.getPhone());
            }
        }
        orderVO.setOrderMapVOList(orderMapVOS);
        return orderVO;
    }

    @Override
    public IPage getOrderList(SearchOrderDTO dto) throws Exception {
        if (dto.getUid() != null) {
            return getUserOrder(dto.getUid(), new PageDTO(dto.getPage(), dto.getSize()));
        }
        OrderTimeEnum instanceByType = OrderTimeEnum.getInstanceByType(dto.getTimeType());
        List<Integer> oids = null;
        if (instanceByType != null) {
            dto.setFrom(instanceByType.getStartTime());
            oids = baseMapper.selectOrderIdWithTimeScope(dto);
        } else if (dto.getFrom() != null) {
            oids = baseMapper.selectOrderIdWithTimeScope(dto);
        } else {
            // 获取所有为完成的订单
            oids = baseMapper.selectIdsByDto(dto);
        }

        // 对oids进行年级，科目过滤，计算总条数
        List<Integer> filterOids = new CopyOnWriteArrayList<>();
        List<Integer> gids = dto.getGids();
        List<Integer> sids = dto.getSids();
        Set<Integer> gidset = new HashSet<>();
        Set<Integer> sidset = new HashSet<>();
        if (!CollectionUtils.isEmpty(gids)) {
            gidset = gids.stream().collect(Collectors.toSet());
        }
        if (!CollectionUtils.isEmpty(sids)) {
            sidset = sids.stream().collect(Collectors.toSet());
        }

        if ((gids != null && gids.size() == gradeMapper.selectCount(null) )||
                (sids != null && sids.size() == subjectMapper.selectCount(null))) {
            filterOids = oids.stream().distinct().collect(Collectors.toList());
            ;
            return doGetOrderList(dto.getPage(), dto.getSize(), filterOids);
        }

        if (CollectionUtils.isEmpty(gids) &&
                CollectionUtils.isEmpty(sids)) {
            filterOids = oids.stream().distinct().collect(Collectors.toList());
            ;
            return doGetOrderList(dto.getPage(), dto.getSize(), filterOids);
        }

        final List<Integer> filteroidscopy = new CopyOnWriteArrayList<>();
        if (!CollectionUtils.isEmpty(gids)) {
            final Set<Integer> gidsetfin = gidset;
            oids.parallelStream().forEach(e -> {
                List<Integer> orderGradeIds = getOrderGradeIds(e);
                if (CollectionUtils.isEmpty(orderGradeIds)) {
                    filteroidscopy.add(e);
                } else {
                    for (Integer orderGradeId : orderGradeIds)
                        if (gidsetfin.contains(orderGradeId)) {
                            filteroidscopy.add(e);
                            break;
                        }
                }
                System.out.println(e);
            });
        }
        if (!CollectionUtils.isEmpty(sids)) {
            final Set<Integer> sidsetfin = sidset;
            oids.parallelStream().forEach(e -> {
                List<Integer> orderSubjectIds = getOrderSubjectIds(e);
                if (CollectionUtils.isEmpty(orderSubjectIds)) {
                    filteroidscopy.add(e);
                } else {
                    for (Integer orderSubjectId : orderSubjectIds) {
                        if (sidsetfin.contains(orderSubjectId)) {
                            filteroidscopy.add(e);
                            break;
                        }
                    }
                }

            });
        }

        filterOids = filteroidscopy.stream().distinct().collect(Collectors.toList());
        return doGetOrderList(dto.getPage(), dto.getSize(), filterOids);

    }

    private IPage doGetOrderList(long pageN, Long size, List<Integer> oids) {
        int fromIndex = (int) ((pageN - 1) * size);
        // 判断fromIndex是否越界
        if (fromIndex > oids.size() - 1) {
            IPage<OrderVO> result = new Page<>(pageN, size);
            result.setTotal(oids.size())
                    .setRecords(null)
                    .setPages(oids.size() / size + 1);
            return result;
        }
        int toIndex = (int) (fromIndex + size);
        if (toIndex > oids.size()) {
            toIndex = oids.size();
        }

        // 截取oids
        List<Integer> selectOids = oids.subList(fromIndex, toIndex);
        List<OrderTable> orderTableList = baseMapper.selectBatchIds(selectOids);
        List<OrderVO> orderVOS = MyBeanUtils.converToList(orderTableList, OrderVO.class);
        IPage<OrderVO> result = new Page<>(pageN, size);
        result.setTotal(oids.size())
                .setRecords(orderVOS)
                .setPages(oids.size() / size + 1);
        for (OrderVO orderVO : orderVOS) {
            User user = userMapper.selectById(orderVO.getPid());
            orderVO.setUserVO(MyBeanUtils.copyProperties2Object(user, UserVO.class));
            String avatar = userMapper.selectAvatarById(orderVO.getPid());
            orderVO.setAvatar(ossUtils.addUrlPrefix(avatar));
        }
        return result;
    }

    /**
     * 更新订单信息
     *
     * @param orderDTO
     * @throws SQLException
     */
    @Override
    @Transactional
    public void updateOrder(OrderDTO orderDTO) throws SQLException {
        // 判断订单状态
        User user = userMapper.selectById(orderDTO.getPid());
        OrderTable orderTable = MyBeanUtils.copyProperties2Object(orderDTO, OrderTable.class);
//        if (orderTable.getFinished() == 1 || orderTable.getFinished() == 3) {
//            throw new MyException("此订单已取消或已完成，无法更改");
//        }
//
//        if (orderTable.getFinished() == 2) {
//            throw new MyException("此订单正在进行中，无法更改");
//        }

        // 判断订单金额的改变
        BigDecimal money = orderDTO.getMoney();
        BigDecimal oldMoney = orderDTO.getOldMoney();
        double v = money.subtract(oldMoney).doubleValue();

//        判断用户余额是否足够
        if (user.getBalance().add(BigDecimal.valueOf(v)).doubleValue() < 0) {
            throw new MyException("账户余额不足，请充值");
        }
        // 新增冻结金额
        user.setBalance(user.getBalance().subtract(BigDecimal.valueOf(v)));
        user.setFreezeBalance(user.getFreezeBalance().add(BigDecimal.valueOf(v)));

        userMapper.updateById(user);

        // 设置gids，sids
        List<Integer> gids = orderDTO.getGids();
        if (!CollectionUtils.isEmpty(gids)) {
            StringBuilder gidstr = new StringBuilder();
            for (int i = 0; i < gids.size(); i++) {
                gidstr.append(gids.get(i) + ";");
            }
            orderTable.setGids(gidstr.toString());
        }

        List<Integer> sids = orderDTO.getSids();
        if (!CollectionUtils.isEmpty(sids)) {
            StringBuilder sidstr = new StringBuilder();
            for (int i = 0; i < sids.size(); i++) {
                sidstr.append(sids.get(i) + ";");
            }
            orderTable.setSids(sidstr.toString());
        }

        boolean b = updateById(orderTable);
        if (!b) {
            throw new SQLException("更新失败");
        }

        // 删除redis中的order的gids和sids
        redisUtils.delete(OrderConstant.ORDER_WITH_GRADE_KEY + orderTable.getId());
        redisUtils.delete(OrderConstant.ORDER_WITH_SUBJECT_KEY + orderTable.getId());
    }

    @Override
    @Transactional
    public void deleteOrder(List<Integer> ids) throws SQLException {
        int i = baseMapper.deleteBatchIds(ids);
        if (i != ids.size()) {
            throw new SQLException("操作失败");
        }
    }

    /**
     * 获取用户订单
     *
     * @param uid
     * @param pageDTO
     * @return
     * @throws Exception
     */
    @Override
    public IPage<OrderVO> getUserOrder(Integer uid, PageDTO pageDTO) throws Exception {
        // 根据uid查询对应的用户信息
        User user = userMapper.selectById(uid);
        if (user == null) {
            throw new SQLException("用户信息不存在");
        }

        if (user.getRoleId().equals(RoleEnum.PARENT.getId())) {
            // 家长的角色
            return getParentOrderList(uid, pageDTO);
            // 获取家长相关信息
        } else if (user.getRoleId().equals(RoleEnum.TEACHER.getId())) {
            // 教师角色
            return getTeacherOrderList(uid, pageDTO);
        }

        return null;
    }

    /**
     * 获取家长角色订单
     *
     * @param uid
     * @param pageDTO
     * @return
     */
    private IPage<OrderVO> getParentOrderList(Integer uid, PageDTO pageDTO) {
        // 查询对象
        IPage<OrderTable> page = new Page<>(pageDTO.getPage(), pageDTO.getSize());
        QueryWrapper<OrderTable> queryWrapper = new QueryWrapper<>();
        // 分页查询用户订单以create_time降序排序
        queryWrapper.eq("pid", uid).orderByAsc("create_time");

        // 返回对象
        IPage<OrderVO> result = new Page<>(pageDTO.getPage(), pageDTO.getSize());

        // 1.获取订单
        page = baseMapper.selectPage(page, queryWrapper);
        result.setTotal(page.getTotal());
        result.setCurrent(page.getCurrent());
        result.setPages(page.getPages());
        result.setSize(page.getSize());
        if (CollectionUtils.isEmpty(page.getRecords())) {
            // 订单记录为空
            return result;
        }
        List<OrderVO> subOrderVo = MyBeanUtils.converToList(page.getRecords(), OrderVO.class);
        // 3.获取订单评价
        setOrderUser(subOrderVo);
        getOrderEvaluate(subOrderVo);
        getSidAndGidTitle(subOrderVo);
        result.setRecords(subOrderVo);
        setAvatar(subOrderVo);
        return result;
    }
    private void setOrderUser(List<OrderVO> orderVOS) {
        for (OrderVO orderVO : orderVOS) {
            User user = userMapper.selectById(orderVO.getPid());
            orderVO.setUserVO(MyBeanUtils.copyProperties2Object(user, UserVO.class));
        }
    }

    private void setAvatar(List<OrderVO> orderVOS) {
        for (OrderVO orderVO : orderVOS) {
            String avatar = userMapper.selectAvatarById(orderVO.getPid());
            orderVO.setAvatar(ossUtils.addUrlPrefix(avatar));
        }
    }

    /**
     * 给订单设置sid文字和gid文字
     *
     * @param orderVOS
     */
    private void getSidAndGidTitle(List<OrderVO> orderVOS) {
        List<Grade> grades = gradeMapper.selectList(null);
        Map<Integer, String> gidTitlemap = new HashMap<>(grades.size());
        for (Grade grade : grades) {
            gidTitlemap.put(grade.getId(), grade.getGradeName());
        }
        List<Subject> subjects = subjectMapper.selectList(null);
        Map<Integer, String> sidTitlemap = new HashMap<>(subjects.size());
        for (Subject subject : subjects) {
            sidTitlemap.put(subject.getId(), subject.getSubjectName());
        }

        for (OrderVO orderVO : orderVOS) {
            String gids = orderVO.getGids();
            String sids = orderVO.getSids();
            if (!StringUtils.isBlank(gids)) {
                String gidsTitle = "";
                String[] split = gids.split(";");
                for (String s : split) {
                    Integer gid = Integer.valueOf(s);
                    gidsTitle += gidTitlemap.get(gid) + "、";
                }
                orderVO.setGradeTitle(gidsTitle);
            }

            if (!StringUtils.isBlank(sids)) {
                String sidsTitle = "";
                String[] split = sids.split(";");
                for (String s : split) {
                    Integer sid = Integer.valueOf(s);
                    sidsTitle += sidTitlemap.get(sid) + "、";
                }
                orderVO.setSubjectTitle(sidsTitle);
            }
        }

        orderVOS.sort((x, y) -> {
            return y.getCreateTime().compareTo(x.getCreateTime());
        });
    }

    /**
     * 获取订单评价
     *
     * @param orderVOS
     */
    private void getOrderEvaluate(List<OrderVO> orderVOS) {
        for (OrderVO orderVO : orderVOS) {
            OrderEvaluate orderEvaluate = orderEvaluateMapper.getByOid(orderVO.getId());
            OrderEvaluateVO orderEvaluateVO = null;
            if (orderEvaluate != null) {
                String[] imgs = orderEvaluate.getImgs().split(";");
                List<String> list = ossUtils.addUrlPrefixArray(imgs);
                orderEvaluateVO = MyBeanUtils.copyProperties2Object(orderEvaluate, OrderEvaluateVO.class);
                orderEvaluateVO.setImgsUrl(list);
            }
            orderVO.setOrderEvaluateVO(orderEvaluateVO);
        }
    }


    /**
     * 订单根据创建时间排序，默认降序排序
     *
     * @param list
     * @param asc  升序
     * @return
     */
    private void sortedByCreateTime(List<OrderVO> list, Boolean asc) {

        Collections.sort(list, (item1, item2) -> {
            long a = item1.getCreateTime().toEpochSecond(ZoneOffset.of("+8"));
            long b = item2.getCreateTime().toEpochSecond(ZoneOffset.of("+8"));
            if (asc != null && asc) {
                // 升序
                return a > b ? 1 : -1;
            } else {
                // 降序
                return b > a ? 1 : -1;
            }
        });
    }

    /**
     * 获取教师角色订单
     *
     * @param uid
     * @param pageDTO
     * @return
     */
    private IPage<OrderVO> getTeacherOrderList(Integer uid, PageDTO pageDTO) {
        IPage<OrderVO> result = new Page<>(pageDTO.getPage(), pageDTO.getSize());

        IPage<OrderMap> orderMapIPage = new Page<>(pageDTO.getPage(), pageDTO.getSize());
        QueryWrapper<OrderMap> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("tid", uid)
                .orderByDesc("create_time");
        // 教师的订单
        orderMapIPage = orderMapMapper.selectPage(orderMapIPage, queryWrapper);
        result.setTotal(orderMapIPage.getTotal())
                .setCurrent(orderMapIPage.getCurrent())
                .setRecords(null);
        if (CollectionUtils.isEmpty(orderMapIPage.getRecords())) {
            // 教师订单为空
            return result;
        }
        List<OrderTable> orderTableList = new ArrayList<>();
        // 订单不为空
        for (OrderMap orderMap : orderMapIPage.getRecords()) {
            Integer oid = orderMap.getOid();
            orderTableList.add(orderTableMapper.selectById(oid));
        }
        List<OrderVO> orderVOS = MyBeanUtils.converToList(orderTableList, OrderVO.class);
        setOrderUser(orderVOS);
        getOrderEvaluate(orderVOS);
        getSidAndGidTitle(orderVOS);
        result.setRecords(orderVOS);
        setAvatar(orderVOS);
        return result;
    }


    /**
     * 获取订单关联的年级
     *
     * @param oid
     * @return
     */
    @Override
    public List<Integer> getOrderGradeIds(Integer oid) {
        // 从redis中获取
        List<Integer> ids = redisUtils.selectList(OrderConstant.ORDER_WITH_GRADE_KEY + oid, Integer.class);
        if (!CollectionUtils.isEmpty(ids)) {
            return ids;
        }
        // 从数据库中获取
        OrderTable orderTable = baseMapper.selectById(oid);
        String gids = orderTable.getGids();
        String[] split = gids.split(";");
        List<Integer> res = new ArrayList<>();
        for (String s : split) {
            res.add(Integer.parseInt(s));
        }
        // 更新redis
        redisUtils.insertList(OrderConstant.ORDER_WITH_GRADE_KEY + oid, res);
        return res;
    }

    /**
     * 获取订单关联的科目
     *
     * @param oid
     * @return
     */
    @Override
    public List<Integer> getOrderSubjectIds(Integer oid) {
        // 从redis中获取
        List<Integer> ids = redisUtils.selectList(OrderConstant.ORDER_WITH_SUBJECT_KEY + oid, Integer.class);
        if (!CollectionUtils.isEmpty(ids)) {
            return ids;
        }
        // 从数据库中获取
        OrderTable orderTable = baseMapper.selectById(oid);
        String sids = orderTable.getSids();
        String[] split = sids.split(";");
        List<Integer> res = new ArrayList<>();
        for (String s : split) {
            res.add(Integer.parseInt(s));
        }
        // 更新redis
        redisUtils.insertList(OrderConstant.ORDER_WITH_SUBJECT_KEY + oid, res);
        return res;
    }

    /**
     * 家长点击完成订单
     * @param tid
     * @param oid
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public void finish(Integer oid, Integer tid) {
        OrderTable orderTable = baseMapper.selectById(oid);
        User parent = userMapper.selectById(orderTable.getPid());
        User teacher = userMapper.selectById(tid);
        //修改用户和教师的冻结账户
        parent.setFreezeBalance(parent.getFreezeBalance().subtract(orderTable.getMoney()));
        teacher.setBalance(teacher.getBalance().add(orderTable.getMoney()));
        teacher.setFreezeBalance(teacher.getFreezeBalance().subtract(orderTable.getMoney()));
        userMapper.updateById(parent);
        userMapper.updateById(teacher);

        // 更新订单状态
        orderTable.setFinished(3);
        baseMapper.updateById(orderTable);
    }

    /**
     * 取消订单
     * @param oid
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public void cancel(Integer oid) {
        baseMapper.cancel(oid);
        //恢复用户冻结金额
        OrderTable orderTable = baseMapper.selectById(oid);
        User user = userMapper.selectById(orderTable.getPid());
        user.setBalance(user.getBalance().add(orderTable.getMoney()));
        user.setFreezeBalance(user.getFreezeBalance().subtract(orderTable.getMoney()));
        userMapper.updateById(user);
    }

    @Override
    public Integer getUserOrderCount(Integer uid) {
        User user = userMapper.selectById(uid);
        if (user.getRoleId().equals(RoleEnum.PARENT.getId())) {
            // 家长订单数
            return baseMapper.getUserOrderCount(uid);
        } else if (user.getRoleId().equals(RoleEnum.TEACHER.getId())) {
            // 教师订单数
            return orderMapMapper.getUserOrderCount(uid);
        }
        return null;
    }

    @Override
    public Integer getCount() {
        QueryWrapper<OrderTable> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("deleted",0);
        Integer integer = baseMapper.selectCount(queryWrapper);
        return integer;
    }
}

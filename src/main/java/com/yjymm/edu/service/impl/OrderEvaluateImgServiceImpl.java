package com.yjymm.edu.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.yjymm.edu.mapper.OrderEvaluateImgMapper;
import com.yjymm.edu.model.entity.OrderEvaluateImg;
import com.yjymm.edu.service.OrderEvaluateImgService;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author yjymm
 * @since 2021-01-30
 */
@Service
public class OrderEvaluateImgServiceImpl extends ServiceImpl<OrderEvaluateImgMapper, OrderEvaluateImg> implements OrderEvaluateImgService {

}

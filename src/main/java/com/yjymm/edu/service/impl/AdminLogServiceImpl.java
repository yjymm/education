package com.yjymm.edu.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.yjymm.edu.exception.MyException;
import com.yjymm.edu.model.dto.AdminLogDTO;
import com.yjymm.edu.model.entity.AdminLog;
import com.yjymm.edu.mapper.AdminLogMapper;
import com.yjymm.edu.service.AdminLogService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author yjymm
 * @since 2021-02-18
 */
@Service
public class AdminLogServiceImpl extends ServiceImpl<AdminLogMapper, AdminLog> implements AdminLogService {

    @Override
    public IPage getList(AdminLogDTO dto) {
        QueryWrapper<AdminLog> queryWrapper = new QueryWrapper<>();
        if (dto.getType() != null) {
            queryWrapper.eq("type", dto.getType());
        }
        if (dto.getState() != null) {
            queryWrapper.eq("state", dto.getState());
        }
        queryWrapper.orderByDesc("create_time");
        IPage<AdminLog> adminLogIPage = baseMapper.selectPage(new Page<>(dto.getPage(), dto.getSize()), queryWrapper);
        return adminLogIPage;
    }


    @Override
    @Transactional(rollbackFor = Exception.class)
    public void read(Integer lid) {
        AdminLog adminLog = baseMapper.selectById(lid);
        adminLog.setState(1);
        baseMapper.updateById(adminLog);
    }
}

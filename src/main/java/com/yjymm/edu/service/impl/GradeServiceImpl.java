package com.yjymm.edu.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.sun.org.apache.xml.internal.serialize.BaseMarkupSerializer;
import com.yjymm.edu.common.MyBeanUtils;
import com.yjymm.edu.exception.MyException;
import com.yjymm.edu.exception.SqlRecordExistsException;
import com.yjymm.edu.mapper.UgtableMapper;
import com.yjymm.edu.mapper.UserMapper;
import com.yjymm.edu.model.entity.Grade;
import com.yjymm.edu.mapper.GradeMapper;
import com.yjymm.edu.model.entity.User;
import com.yjymm.edu.model.vo.GradeVO;
import com.yjymm.edu.service.GradeService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.yjymm.edu.service.UgtableService;
import org.springframework.cache.interceptor.CacheOperationInvoker;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.sql.SQLException;
import java.util.List;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author yjymm
 * @since 2020-12-25
 */
@Service
public class GradeServiceImpl extends ServiceImpl<GradeMapper, Grade> implements GradeService {

    @Resource
    private UserMapper userMapper;

    @Resource
    private UgtableMapper ugtableMapper;

    @Resource
    private UgtableService ugtableService;

    /**
     * 插入grade
     * @param grade
     * @return
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public Boolean insert(Grade grade) {
        QueryWrapper<Grade> wrapper = new QueryWrapper<>();
        wrapper.eq("grade_name", grade.getGradeName());
        Integer integer = baseMapper.selectCount(wrapper);
        if (integer != null && integer > 0) {
            throw new SqlRecordExistsException("该数据已存在");
        }
        int insert = baseMapper.insert(grade);
        if (insert == 1) {
            return Boolean.TRUE;
        }
        return Boolean.FALSE;
    }


    /**
     * 删除
     * @param gid
     * @return
     * @throws SQLException
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public Boolean delete(Integer gid) throws SQLException {
        int i = baseMapper.deleteById(gid);
        if (i != 1) {
            throw new SQLException("删除失败");
        }
        return Boolean.TRUE;
    }



    /**
     * 查询所有年级，用户感兴趣的年级就 ischecked = true
     * @param openId
     * @return
     */
    @Override
    public List<GradeVO> listUserFavoriteByOpenId(String openId) {
        User user = userMapper.selectByOpenId(openId);
        List<Grade> grades = baseMapper.selectList(null);
        List<GradeVO> gradeVOS = MyBeanUtils.converToList(grades, GradeVO.class);

        // 查找用户感兴趣的年级id
        List<Integer> list = ugtableService.getUserFarvoriteGradeIds(user.getId());
        if (list == null || list.size() == 0) {
            return gradeVOS;
        }
        gradeVOS.stream().forEach( item ->{
            if (list.contains(item.getId())){
                item.setIsChecked(true);
            }
        });
        return gradeVOS;
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public int deleteBatchById(List<Integer> gids) {
        int count = 0;
        Grade grade = new Grade();
        for (Integer gid : gids) {
            grade.setId(gid);
            grade.setDeleted(1);
            count += baseMapper.updateById(grade);
        }
        return count;
    }

    @Transactional(rollbackFor = Exception.class)
    @Override
    public void insertGrade(Grade grade) throws Exception{
        Integer i = baseMapper.findByName(grade.getGradeName());
        if (i >= 1) {
            throw new SqlRecordExistsException("记录以存在，添加失败");
        }
        grade.setDeleted(0);
        int insert = baseMapper.insert(grade);
        if (insert != 1) {
            throw new MyException("插入记录失败");
        }
    }

    @Transactional(rollbackFor = Exception.class)
    @Override
    public void updateGrade(Grade grade) {
        Integer byName = baseMapper.findByName(grade.getGradeName());
        if (byName >= 1) {
            throw new MyException("该信息已存在，请重新输入");
        }
        baseMapper.updateById(grade);
    }
}

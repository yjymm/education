package com.yjymm.edu.service;

import com.yjymm.edu.model.dto.RechargeDTO;
import com.yjymm.edu.model.entity.Recharge;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author yjymm
 * @since 2020-12-25
 */
public interface RechargeService extends IService<Recharge> {

    Recharge insert(RechargeDTO rechargeDTO) throws Exception;

    void doRecharge(Integer rid, Integer uid, Integer rechargeCode) throws Exception;

    List<Recharge> getListByUid(Integer uid);

    void sortedByCreateTime(List<Recharge> list);

    Boolean checkIsOverTime(Recharge recharge);

    Boolean pay(String rechargeNum);

    Integer getUserRechargeCount(Integer id);

    Integer getUserSuccessRechargeCount(Integer id);

    Integer getCount();
}

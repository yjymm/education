package com.yjymm.edu.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.yjymm.edu.model.dto.OrderDTO;
import com.yjymm.edu.model.dto.PageDTO;
import com.yjymm.edu.model.dto.SearchOrderDTO;
import com.yjymm.edu.model.entity.OrderTable;
import com.baomidou.mybatisplus.extension.service.IService;
import com.yjymm.edu.model.vo.OrderVO;

import java.sql.SQLException;
import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author yjymm
 * @since 2020-12-25
 */
public interface OrderTableService extends IService<OrderTable> {

    void dispatchOrder(OrderDTO orderDTO) throws SQLException;

    OrderVO getOrder(Integer id) throws JsonProcessingException;

    IPage getOrderList(SearchOrderDTO searchOrderDTO) throws Exception;

    void updateOrder(OrderDTO orderDTO) throws SQLException;

    void deleteOrder(List<Integer> ids) throws SQLException;

    IPage<OrderVO> getUserOrder(Integer uid, PageDTO pageDTO) throws Exception;

    List<Integer> getOrderGradeIds(Integer oid);

    List<Integer> getOrderSubjectIds(Integer oid);

    void finish(Integer integer, Integer oid);

    void cancel(Integer oid);

    Integer getUserOrderCount(Integer id);

    Integer getCount();
}

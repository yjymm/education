package com.yjymm.edu.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.yjymm.edu.model.dto.SearchOrderDTO;
import com.yjymm.edu.model.dto.SearchTeacherDTO;
import com.yjymm.edu.model.dto.SearchUserDTO;

/**
 * @author yjymm
 * @date 2021-01-05 17:25
 */
public interface SearchService {
    IPage search(SearchTeacherDTO searchTeacherDTO);

    IPage searchTeacherWithTeacherName(long pageN, long size, String teacherName);

    IPage getFarvoriteTeacher(SearchTeacherDTO uid);

    IPage searchUser(SearchUserDTO dto);

//    IPage searchOrder(SearchOrderDTO dto);
}
